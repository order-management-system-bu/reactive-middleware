#!/usr/bin/env bash
cd rmw-core/
mvn antrun:run@ktlint-format
cd ../rmw-om/
mvn antrun:run@ktlint-format
cd ../rmw-urm/
mvn antrun:run@ktlint-format
cd ../