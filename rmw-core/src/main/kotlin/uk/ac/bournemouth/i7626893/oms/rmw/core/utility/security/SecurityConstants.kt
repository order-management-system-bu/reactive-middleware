package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security

/**
 * Object is responsible for providing all security-related
 * constants.
 */
object SecurityConstants {
    const val SECURITY_KEY: String = "jznz8A2udZLZmP7wx2WJmn5nXxRGaRLGJTeGkvd46wwbftcfakvujEqusfAb6znHSp2FAJCqUx6p4" +
        "LL63d4e5CQ6skScBv9ZN7VXu27L57ug3KE7BxjCwFeKhhEvRrcpJnjYUUKZr9MCkJWYZ5a2GQhHYxZdrFZXPHZJX4cy38kUEb7hfFWP6A6" +
        "kvBbxcrjqqMC43AfJ8L8KaUXgnsDsrMnrHFgnMk22T8bDyzRJNX4hayrxpY7rSppQksydge6dS5WxdE8f8BCxVJ5aPB84EZqFt2vyfst9Z" +
        "3tgGpLqT5SGUTGzq57M9npgyjgsxyz26kNfp82gVfH2EhFjkBEJRvhZaxM66a6kwj72GXpVV9SbHeULJ85vrwaz2qg5htnaXnuSd3s98NQ" +
        "kLKANXCs9DYMaShfPNnQmx9ne5EfG7z7aK8qrLV3TeKWWauTkEZcgLVATG5YWS5jYJN8dxHNWMrBXeR5dLSSA7Q6KnrucWLGgBcqamnJ5E" +
        "cM4UxH6Sqbg"
}