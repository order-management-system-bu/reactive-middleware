package uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications

/**
 * Model represents a single CustomerOrderCreationNotification from the
 * OMS-ENotifications application.
 */
data class OrderCreationNotification(
    val target_email: String,
    val order_number: String,
    val products: List<Pair<String, String>>,
    val contact_number: String,
    val street: String,
    val city: String,
    val county: String,
    val postcode: String
)