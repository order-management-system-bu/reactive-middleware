package uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications

/**
 * Model represents a single CustomerModificationNotification from the
 * OMS-ENotifications application.
 */
data class CustomerModificationNotification(
    val target_email: String,
    val full_name: String,
    val date_updated: String,
    val field_names: List<String>
)