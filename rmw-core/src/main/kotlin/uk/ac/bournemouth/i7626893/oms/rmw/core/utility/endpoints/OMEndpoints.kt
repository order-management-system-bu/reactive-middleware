package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints

/**
 * Utility class is responsible for providing all routing paths
 * associated with the CRM module.
 */
class OMEndpoints(private val debug: Boolean = false) {
    fun removeMenuItemByCategory(categoryId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/om/menu-item/$categoryId/delete/category"
        else TODO("Implement when RMW is deployed")

    fun viewMenuItemById(menuItemId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/om/menu-item/$menuItemId/view"
        else TODO("Implement when RMW is deployed")

    fun viewMenuOptionsByIds(menuOptionIds: List<String>): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/om/menu-item-option/${menuOptionIds.joinToString().replace("\\s".toRegex(), "")}/list"
        else TODO("Implement when RMW is deployed")

    fun viewDiscountCodeByCode(discountCode: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/om/discount-code/$discountCode/view/code"
        else TODO("Implement when RMW is deployed")

    fun viewDeliveryOptions(): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/om/delivery-option/view"
        else TODO("Implement when RMW is deployed")
}