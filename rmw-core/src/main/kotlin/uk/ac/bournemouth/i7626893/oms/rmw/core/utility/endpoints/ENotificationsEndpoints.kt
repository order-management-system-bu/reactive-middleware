package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints

/**
 * Utility class is responsible for providing all routing paths
 * associated with the OMS-ENotifications application.
 */
class ENotificationsEndpoints(private val debug: Boolean = false) {
    fun createCustomerActivationEndpoint(): String =
        if (debug) "http://127.0.0.1:8081/notification/customer-activation/"
        else "https://e-notifications-230119.appspot.com/notification/customer-activation/"

    fun createCustomerModificationEndpoint(): String =
        if (debug) "http://127.0.0.1:8081/notification/customer-modification/"
        else "https://e-notifications-230119.appspot.com/notification/customer-modification/"

    fun createDiscountCodeEndpoint(): String =
        if (debug) "http://127.0.0.1:8081/notification/discount-code/"
        else "https://e-notifications-230119.appspot.com/notification/discount-code/"

    fun createManagerEndpoint(): String =
        if (debug) "http://127.0.0.1:8081/notification/manager/"
        else "https://e-notifications-230119.appspot.com/notification/manager/"

    fun createOrderEndpoint(): String =
        if (debug) "http://127.0.0.1:8081/notification/order-creation/"
        else "https://e-notifications-230119.appspot.com/notification/order-creation/"
}