package uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications

/**
 * Model represents a single DiscountCodeNotification from the
 * OMS-ENotifications application.
 */
data class DiscountCodeNotification(
    val target_email: List<String>,
    val discount_title: String,
    val discount_code: String
)