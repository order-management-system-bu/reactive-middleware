package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureStore
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader

/**
 * Interface is responsible for exposing a secure generic creation
 * endpoint to external entities for instances of M.
 */
interface SecureStore<M : Model<M>> : UnsecureStore<M>, Dependency.Permission {

    /**
     * Secure endpoint is responsible for storing a single instance of M
     * to the applications database.
     */
    override fun store(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.$permission.store") {
            super.store(request)
        }
}