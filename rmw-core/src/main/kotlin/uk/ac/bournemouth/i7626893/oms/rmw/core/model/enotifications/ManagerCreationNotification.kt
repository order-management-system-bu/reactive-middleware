package uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications

/**
 * Model represents a single ManagerCreationNotification from the
 * OMS-ENotifications application.
 */
data class ManagerCreationNotification(
    val target_email: String,
    val password: String
)