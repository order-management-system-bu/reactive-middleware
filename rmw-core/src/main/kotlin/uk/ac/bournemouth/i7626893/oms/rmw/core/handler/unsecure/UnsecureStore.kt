package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.validate

/**
 * Interface is responsible for exposing a generic creation endpoint
 * to external entities for instances of M.
 */
interface UnsecureStore<M : Model<M>> :
    Dependency.Repository<M>,
    Dependency.StoredModelClass<M>,
    Dependency.CreateValidator<M> {

    /**
     * Endpoint is responsible for storing a single instance of M
     * to the applications database.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .bodyToMono(mClass)
        .validate(cValidator) { m -> repository.store(m).flatMap { sm -> ResponseBuilder.createdResponse(sm.id!!) } }
}