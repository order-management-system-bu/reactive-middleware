package uk.ac.bournemouth.i7626893.oms.rmw.core.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault

/**
 * Abstract class represents a Validator class that is
 * responsible for verifying the integrity of provided
 * instances of M.
 */
abstract class AbstractValidator<M : Model<M>>(
    private val validateMandatoryFields: Boolean
) {
    fun validate(m: M): List<ValidationFault> = validateFields(m) +
        (validateMandatoryFields(m).takeIf { validateMandatoryFields } ?: emptyList())

    protected abstract fun validateFields(m: M): List<ValidationFault>

    protected abstract fun validateMandatoryFields(m: M): List<ValidationFault>
}