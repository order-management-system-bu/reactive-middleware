package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader

/**
 * Interface is responsible for exposing a secure generic delete
 * endpoint to external entities for instances of M.
 */
interface SecureDelete<M : Model<M>> : UnsecureDelete<M>, Dependency.Permission {

    /**
     * Secure endpoint is responsible for removing a single instance of M
     * from the applications database.
     */
    override fun delete(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.$permission.delete") {
            super.delete(request)
        }
}