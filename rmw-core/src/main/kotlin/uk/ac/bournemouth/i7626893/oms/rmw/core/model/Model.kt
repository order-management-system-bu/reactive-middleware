package uk.ac.bournemouth.i7626893.oms.rmw.core.model

/**
 * Interface represents a single storable Model associated
 * with the application.
 */
interface Model<M : Model<M>> {
    val id: String?

    /**
     * Function is responsible for taking an instance of M and
     * updating it with fields from this.
     */
    fun update(m: M): M = m
}