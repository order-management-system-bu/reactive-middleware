package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation

/**
 * Object is responsible for providing common REGEX expressions
 * used for validation.
 */
object ValidationRegex {
    val ALPHA: Regex = Regex("""^[A-Za-z]*""")
    val ALPHA_W_SPACE: Regex = Regex("""^[A-Za-z ]*""")
    val ALPHANUMERIC: Regex = Regex("""^[A-Za-z0-9]*""")
    val ALPHANUMERIC_W_SPACE: Regex = Regex("""^[A-Za-z0-9 ]*""")
    val SENTENCE: Regex = Regex("""^[A-Za-z0-9 ,.£]*""")
    val OBJECT_ID: Regex = Regex("""^[a-f\d]{24}$""")
    val PASSWORD: Regex = Regex("""^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$""")
    val MOBILE_NUMBER: Regex = Regex(
        """^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?${'$'}""")
}