package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder

/**
 * Interface is responsible for exposing a generic list endpoint
 * to external entities for instance of M.
 */
interface UnsecureList<M : Model<M>> :
    Dependency.Repository<M>,
    Dependency.StoredModelClass<M> {

    /**
     * Endpoint is responsible for retrieving a collection M instances
     * from the applications database.
     */
    fun list(request: ServerRequest): Mono<ServerResponse> = repository
        .list(mClass)
        .collectList()
        .flatMap { m -> ResponseBuilder.okResponse(m) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}