package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints

/**
 * Utility class is responsible for providing all routing paths
 * associated with external services used by the application.
 */
class ExternalEndpoints(private val debug: Boolean = false) {
    // Postcode lookup API is provided by https://postcodes.io/
    fun lookupPostcode(postcode: String): String = "http://api.postcodes.io/postcodes/$postcode"
}