package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder

private const val ERROR_NO_AUTHENTICATION_TOKEN: String = "The request did not contain a valid authentication token"
private const val ERROR_INSUFFICIENT_PERMISSIONS: String = "The user does not have the necessary permissions to access this resource"

/**
 * Extension function is responsible for validating a requests
 * authorization header.
 */
fun ServerRequest.validateAuthenticationHeader(
    permission: String,
    onSuccess: () -> Mono<ServerResponse>
): Mono<ServerResponse> = this.headers().header("Authorization").firstOrNull()?.let { header ->
    val maybeFault: Pair<String, HttpStatus>? = try {
        val key = Jwts
            .parser()
            .setSigningKey(SecurityConstants.SECURITY_KEY)
            .parseClaimsJws(header)
            .body
        if (!key.get("permissions", List::class.java).contains(permission))
            Pair(ERROR_INSUFFICIENT_PERMISSIONS, HttpStatus.FORBIDDEN)
        else null
    } catch (e: JwtException) {
        Pair(ERROR_NO_AUTHENTICATION_TOKEN, HttpStatus.UNAUTHORIZED)
    }
    if (maybeFault == null) onSuccess()
    else ResponseBuilder.buildResponse(maybeFault.second, maybeFault.first)
} ?: ResponseBuilder.buildResponse(HttpStatus.UNAUTHORIZED, ERROR_NO_AUTHENTICATION_TOKEN)