package uk.ac.bournemouth.i7626893.oms.rmw.core.repository

import com.mongodb.client.result.DeleteResult
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * AbstractRepository class is responsible for providing CRUD functionality
 * for instances of M in the applications database.
 */
abstract class AbstractRepository<M : Model<M>>(
    private val template: ReactiveMongoTemplate
) {
    /**
     * Function is responsible for retrieving all stored instances
     * of M from the applications database.
     */
    fun list(clazz: Class<M>): Flux<M> = template.findAll(clazz)

    /**
     * Function is responsible for finding a single instance of M
     * from the applications database based on the provided ID.
     */
    fun findById(id: String, clazz: Class<M>): Mono<M> = template.findById(id, clazz)

    /**
     * Function is responsible for storing a single instance of M
     * to the applications database.
     */
    fun store(model: M): Mono<M> = template.save(model)

    /**
     * Function is responsible for removing a single instance of M
     * from the applications database.
     */
    fun delete(model: M): Mono<DeleteResult> = template.remove(model)
}