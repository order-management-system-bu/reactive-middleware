package uk.ac.bournemouth.i7626893.oms.rmw.core.type

/**
 * Class, inspired by Scala's Either Monad represents an instance of either
 * L or R
 */
sealed class Either<out L, out R> {
    fun isLeft(): Boolean = this is Left<L>

    fun isRight(): Boolean = this is Right<R>

    fun <X> fold(left: (L) -> X, right: (R) -> X): X = when (this) {
        is Left -> left(this.left)
        is Right -> right(this.right)
    }
}

/**
 * Data class represents the left side of an Either type.
 */
data class Left<out L>(val left: L) : Either<L, Nothing>()

/**
 * Data class represents the right side of an Either type.
 */
data class Right<out R>(val right: R) : Either<Nothing, R>()