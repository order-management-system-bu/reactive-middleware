package uk.ac.bournemouth.i7626893.oms.rmw.core.handler

import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator

/**
 * Object is responsible for providing class dependencies
 * to handler interfaces.
 */
object Dependency {
    interface Repository<M : Model<M>> {
        val repository: AbstractRepository<M>
    }

    interface StoredModelClass<M : Model<M>> {
        val mClass: Class<M>
    }

    interface CreateValidator<M : Model<M>> {
        val cValidator: AbstractValidator<M>
    }

    interface UpdateValidator<M : Model<M>> {
        val uValidator: AbstractValidator<M>
    }

    interface Permission {
        val permission: String
    }
}