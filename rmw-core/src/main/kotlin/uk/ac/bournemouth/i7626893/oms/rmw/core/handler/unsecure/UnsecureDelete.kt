package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants

/**
 * Interface is responsible for exposing a generic deletion endpoint
 * to external entities for instances of M.
 */
interface UnsecureDelete<M : Model<M>> :
    Dependency.Repository<M>,
    Dependency.StoredModelClass<M> {

    /**
     * Endpoint is responsible for removing a single instance of M
     * from the applications database.
     */
    fun delete(request: ServerRequest): Mono<ServerResponse> = repository
        .findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), mClass)
        .flatMap { m -> repository.delete(m).flatMap { ResponseBuilder.okResponse(Unit) } }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}