package uk.ac.bournemouth.i7626893.oms.rmw.core

import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.context.support.beans
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ENotificationsEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ExternalEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.OMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.URMEndpoints

/**
 * Object is responsible for providing module-level
 * configuration for the Core module.
 */
object CoreModule {
    // Const is responsible for defining the applications debug state.
    const val DEBUG_MODE: Boolean = true

    fun beans(): BeanDefinitionDsl = beans {
        bean { ENotificationsEndpoints(DEBUG_MODE) }
        bean { URMEndpoints(DEBUG_MODE) }
        bean { OMEndpoints(DEBUG_MODE) }
        bean { ExternalEndpoints(DEBUG_MODE) }
    }
}