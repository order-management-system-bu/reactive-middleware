package uk.ac.bournemouth.i7626893.oms.rmw.core.utility

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

/**
 * Object is responsible for providing ServerResponse
 * construction functions.
 */
object ResponseBuilder {

    fun <X : Any> buildResponse(status: HttpStatus, x: X): Mono<ServerResponse> = ServerResponse
        .status(status)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .body(BodyInserters.fromObject(x))

    fun <X : Any> okResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.OK, x)
    fun <X : Any> createdResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.CREATED, x)

    fun <X : Any> badRequestResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.BAD_REQUEST, x)
    fun <X : Any> unauthorizedResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.UNAUTHORIZED, x)
    fun <X : Any> notFoundResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.NOT_FOUND, x)

    fun <X : Any> internalErrorResponse(x: X): Mono<ServerResponse> = buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, x)
}