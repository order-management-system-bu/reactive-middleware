package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault

/**
 * Object is responsible for providing common validation
 * fault messages.
 */
object ValidationFaults {

    fun missingField(fieldName: String): ValidationFault = ValidationFault(
        fieldName, "This field was expected but was not provided."
    )

    fun invalidCharacters(fieldName: String): ValidationFault = ValidationFault(
        fieldName, "This field contained one or more invalid characters."
    )

    fun exceededMaxLength(fieldName: String, maxLength: Int): ValidationFault = ValidationFault(
        fieldName, "This field exceeded the maximum length of $maxLength"
    )
}