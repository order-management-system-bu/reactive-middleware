package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler

/**
 * Object is responsible for providing handler-related constants.
 */
object HandlerConstants {
    const val PATH_VAR_ENTITY_ID: String = "id"
}