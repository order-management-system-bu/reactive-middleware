package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation

import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator

/**
 * Extension function is responsible for validating instances
 * of M with the provided validator.
 */
fun <M : Model<M>> Mono<M>.validate(
    validator: AbstractValidator<M>,
    onSuccess: (M) -> Mono<ServerResponse>
): Mono<ServerResponse> = this.flatMap { m ->
    val result = validator.validate(m)
    if (result.isNotEmpty())
        ResponseBuilder.badRequestResponse(result)
    else
        onSuccess(m)
}