package uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications

/**
 * Model represents a single UserActivationNotification from the
 * OMS-ENotifications application.
 */
data class UserActivationNotification(
    val target_email: String,
    val message: String
)