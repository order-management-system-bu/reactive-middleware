package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader

/**
 * Interface is responsible for exposing a secure generic list endpoint
 * to external entities for instances of M.
 */
interface SecureList<M : Model<M>> : UnsecureList<M>, Dependency.Permission {

    /**
     * Secure endpoint is responsible for retrieving a collection of M instances
     * from the applications database.
     */
    override fun list(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.$permission.list") {
            super.list(request)
        }
}