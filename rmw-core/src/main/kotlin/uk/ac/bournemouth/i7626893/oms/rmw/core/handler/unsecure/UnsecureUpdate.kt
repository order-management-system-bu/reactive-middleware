package uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.Dependency
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.validate

/**
 * Interface is responsible for exposing a generic update endpoint
 * to external entities for instances of M.
 */
interface UnsecureUpdate<M : Model<M>> :
    Dependency.Repository<M>,
    Dependency.StoredModelClass<M>,
    Dependency.UpdateValidator<M> {

    /**
     * Endpoint is responsible for updating an instance of M stored
     * in the applications database.
     */
    fun update(request: ServerRequest): Mono<ServerResponse> = request
        .bodyToMono(mClass)
        .validate(uValidator) { m ->
            repository
                .findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), mClass)
                .flatMap { sm -> repository.store(m.update(sm)).flatMap { ResponseBuilder.okResponse(Unit) } }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }
}