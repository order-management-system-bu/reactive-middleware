package uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints

/**
 * Utility class is responsible for providing all routing paths
 * associated with the CRM module.
 */
class URMEndpoints(private val debug: Boolean = false) {
    fun createUser(): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user"
        else TODO("Implement when RMW is deployed")

    fun viewUser(userId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user/$userId/view"
        else TODO("Implement when RMW is deployed")

    fun updateUser(userId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user/$userId/update"
        else TODO("Implement when RMW is deployed")

    fun deleteUser(userId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user/$userId/delete"
        else TODO("Implement when RMW is deployed")

    fun activateUser(userId: String): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user/$userId/activate"
        else TODO("Implement when RMW is deployed")

    fun listContactableCustomerEmails(): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/customer/list/email"
        else TODO("Implement when RMW is deployed")

    fun listUserEmails(userIds: List<String>): String =
        if (debug) "http://127.0.0.1:9000/oms/rmw/urm/user/${userIds.joinToString().replace("\\s".toRegex(), "")}/list"
        else TODO("Implement when RMW is deployed")
}