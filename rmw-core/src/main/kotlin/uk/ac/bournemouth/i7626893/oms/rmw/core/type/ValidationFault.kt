package uk.ac.bournemouth.i7626893.oms.rmw.core.type

/**
 * Type represents a single validation fault.
 */
data class ValidationFault(
    val field: String,
    val fault: String
)