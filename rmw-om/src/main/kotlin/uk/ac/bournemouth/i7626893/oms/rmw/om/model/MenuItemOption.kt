package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored MenuItemOption.
 */
@Document(collection = "menu_item_options")
data class MenuItemOption(
    @Id override val id: String? = null,
    val title: String? = null,
    val price: Double? = null
) : Model<MenuItemOption> {
    override fun update(m: MenuItemOption): MenuItemOption = m.copy(
        title = title.takeIf { !it.isNullOrBlank() } ?: m.title,
        price = price.takeIf { it != null } ?: m.price
    )
}