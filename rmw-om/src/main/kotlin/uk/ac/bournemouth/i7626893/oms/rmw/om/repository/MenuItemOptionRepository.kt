package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOption

/**
 * AbstractRepository class is responsible for providing the necessary
 * CRUD functionality for the menu_item_options collection.
 */
class MenuItemOptionRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<MenuItemOption>(template) {

    /**
     * Function is responsible for retrieving all menu item options
     * with the provided ids.
     */
    fun findByIds(ids: List<String>): Flux<MenuItemOption> = template
        .find(BasicQuery("{\"id\": {\"\$in\": ${ids.map { "\"$it\"" }}}}"), MenuItemOption::class.java)
}