package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications.OrderCreationNotification
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.Either
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.Left
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.Right
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ENotificationsEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.OMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DeliveryOption
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DiscountCode
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.InOrder
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItem
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOption
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.Order
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.OrderStatus
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.OrderRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.OrderValidator

typealias MenuAndOptionsPair = Pair<MenuItem, List<MenuItemOption>>

/**
 * Handler class is responsible for exposing Order-related endpoints
 * to external entities.
 */
class OrderHandler(
    override val repository: OrderRepository,
    private val cValidator: OrderValidator,
    private val omEndpoints: OMEndpoints,
    private val eNotificationsEndpoints: ENotificationsEndpoints,
    private val webClient: WebClient
) : SecureView<Order>,
    SecureList<Order> {

    override val permission: String get() = "om.order"
    override val mClass: Class<Order> get() = Order::class.java

    /**
     * Endpoint is responsible for creating new orders.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request.bodyToMono(InOrder::class.java).flatMap { inOrder ->
        val validationFaults = cValidator.validate(inOrder)
        if (validationFaults.isNotEmpty())
            ResponseBuilder.badRequestResponse(validationFaults)
        else webClient.get().uri(omEndpoints.viewDeliveryOptions()).retrieve().bodyToMono<DeliveryOption>()
            .flatMap { deliveryOption ->
                Flux.fromIterable(inOrder.orderOptions).flatMap { orderOption ->
                    if (orderOption.selectedOptionIds.isNotEmpty())
                        webClient.get().uri(omEndpoints.viewMenuItemById(orderOption.menuItemId!!)).retrieve()
                            .bodyToMono<MenuItem>().flatMap { menuItem ->
                                webClient.get().uri(omEndpoints.viewMenuOptionsByIds(orderOption.selectedOptionIds)).retrieve()
                                    .bodyToMono<List<MenuItemOption>>().flatMap { menuItemOptions ->
                                        MenuAndOptionsPair(menuItem, menuItemOptions).toMono()
                                    }
                            }
                    else webClient.get().uri(omEndpoints.viewMenuItemById(orderOption.menuItemId!!))
                        .retrieve().bodyToMono<MenuItem>().flatMap { menuItem -> MenuAndOptionsPair(menuItem, emptyList()).toMono() }
                }.collectList().flatMap { menuAndOptionPairs ->
                    val basketPrice: Double = menuAndOptionPairs.map { menuAndOptionPair ->
                        menuAndOptionPair.first.price!! + menuAndOptionPair.second.map { it.price!! }.sum()
                    }.sum()
                    if (deliveryOption.minimumForDelivery != null && basketPrice < deliveryOption.minimumForDelivery)
                        ResponseBuilder.badRequestResponse(errorMinimumSpendNotReached(basketPrice - deliveryOption.minimumForDelivery))
                    else run {
                        if (!inOrder.discountCode.isNullOrBlank())
                            webClient.get().uri(omEndpoints.viewDiscountCodeByCode(inOrder.discountCode!!))
                                .exchange().flatMap<Either<Mono<ServerResponse>, Double>> { clientResponse ->
                                    when (clientResponse.statusCode()) {
                                        HttpStatus.OK -> clientResponse.bodyToMono<DiscountCode>().flatMap { discountCode ->
                                            if (!discountCode.enabled)
                                                Left(ResponseBuilder.badRequestResponse(ValidationFault(
                                                    "discountCode", ERROR_DISCOUNT_CODE_NOT_ACTIVE
                                                ))).toMono()
                                            else if (discountCode.spendEligibility != null && basketPrice < discountCode.spendEligibility)
                                                Left(ResponseBuilder.badRequestResponse(ValidationFault(
                                                    "discountCode", errorDiscountCodeNotSpendEnough(discountCode.spendEligibility - basketPrice)
                                                ))).toMono()
                                            else Right(when (discountCode.discountType) {
                                                "PERCENTAGE" -> (basketPrice - (basketPrice * (discountCode.discount / 100)))
                                                else -> (basketPrice - discountCode.discount) // Fixed discount.
                                            }).toMono()
                                        }

                                        HttpStatus.NOT_FOUND -> Left(ResponseBuilder.badRequestResponse(ValidationFault(
                                            "discountCode", "The provided discount code was not found."
                                        ))).toMono()

                                        else -> Left(ResponseBuilder.internalErrorResponse(Unit)).toMono()
                                    }
                                }
                        else Right(0.00).toMono()
                    }.flatMap { errorOrPrice ->
                        errorOrPrice.fold({ it }, { price ->
                            val discountedPrice = price + (deliveryOption?.deliveryCharge ?: 0.00)
                            repository.store(Order(
                                customerId = inOrder.customerId,
                                customerEmail = inOrder.customerEmail!!,
                                customerName = inOrder.customerName!!,
                                customerContactNumber = inOrder.customerContactNumber!!,
                                orderStatus = OrderStatus.ACCEPTED,
                                price = discountedPrice,
                                orderOptions = inOrder.orderOptions,
                                street = inOrder.street!!,
                                city = inOrder.city!!,
                                county = inOrder.county!!,
                                postcode1 = inOrder.postcode1!!,
                                postcode2 = inOrder.postcode2!!,
                                discountCode = inOrder.discountCode
                            )).flatMap { storedOrder ->
                                webClient.post().uri(eNotificationsEndpoints.createOrderEndpoint())
                                    .body(BodyInserters.fromObject(OrderCreationNotification(
                                        target_email = storedOrder.customerEmail,
                                        order_number = storedOrder.id!!,
                                        products = menuAndOptionPairs.map { Pair(it.first.title!!, (it.first.price!! + it.second.map { option -> option.price!! }.sum()).toString()) },
                                        contact_number = storedOrder.customerContactNumber,
                                        street = storedOrder.street,
                                        city = storedOrder.city,
                                        county = storedOrder.county,
                                        postcode = storedOrder.postcode1 + " " + storedOrder.postcode2
                                    )))
                                    .exchange().flatMap { ResponseBuilder.okResponse(storedOrder.id) }
                            }
                        })
                    }
                }
            }
    }

    /**
     * Endpoint is responsible for updating an orders order status.
     */
    fun updateOrderStatus(request: ServerRequest): Mono<ServerResponse> =
        request.validateAuthenticationHeader("oms.rmw.om.order.update") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), Order::class.java).flatMap { order ->
                try {
                    repository.store(order.copy(orderStatus = OrderStatus.valueOf(request.pathVariable("status"))))
                        .flatMap { ResponseBuilder.okResponse(Unit) }
                } catch (e: IllegalArgumentException) {
                    ResponseBuilder.badRequestResponse(ValidationFault("orderStatus", "The provided order status was unrecognised."))
                }
            }
        }

    /**
     * Endpoint is responsible for retrieving all currently stored
     * Orders based on the provided status.
     */
    fun listOrdersByStatus(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.order.list") {
            repository.listByOrderStatuses(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID).split(","))
                .collectList()
                .flatMap { ResponseBuilder.okResponse(it) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving all currently stored
     * orders associated with the provided delivery driver.
     */
    fun listOrdersByDeliveryDriver(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.order.list") {
            repository.listByDeliveryDriver(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID)).collectList()
                .flatMap { ResponseBuilder.okResponse(it) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving all currently stored
     * orders associated with the provided customer.
     */
    fun listOrdersByCustomer(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.order.list.customer") {
            repository.listByCustomer(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID)).collectList()
                .flatMap { ResponseBuilder.okResponse(it) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for updating the delivery driver status
     * of an order.
     */
    fun assignDeliveryDriver(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.order.driver.assign") {
            request.bodyToMono(InOrder::class.java).flatMap { inOrder ->
                if (inOrder.deliveryDriver != null && inOrder.deliveryDriver.matches(ValidationRegex.OBJECT_ID))
                    repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), Order::class.java).flatMap { order ->
                        repository.store(order.copy(deliveryDriver = inOrder.deliveryDriver)).flatMap { ResponseBuilder.okResponse(Unit) }
                    }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
                else ResponseBuilder.badRequestResponse(listOf(ValidationFault("deliveryDriver", "Invalid delivery driver.")))
            }
        }

    companion object {
        private const val ERROR_DISCOUNT_CODE_NOT_ACTIVE: String =
            "Unfortunately that discount code is no longer available."

        private fun errorMinimumSpendNotReached(leftToSpend: Double): String =
            "The minimum spend for delivery has not been reached. You must spend another: £$leftToSpend"

        private fun errorDiscountCodeNotSpendEnough(leftToSpend: Double): String =
            "To claim this discount, you must spend another: £$leftToSpend"
    }
}