package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureStore
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOption
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemOptionRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemOptionValidator

/**
 * Handler class is responsible for exposing MenuItemOption-related endpoints
 * to external entities.
 */
class MenuItemOptionHandler(
    override val repository: MenuItemOptionRepository,
    override val cValidator: MenuItemOptionValidator,
    override val uValidator: MenuItemOptionValidator
) :
    SecureStore<MenuItemOption>,
    SecureUpdate<MenuItemOption>,
    SecureDelete<MenuItemOption>,
    UnsecureView<MenuItemOption>,
    UnsecureList<MenuItemOption> {

    override val permission: String get() = "om.menuitemoption"
    override val mClass: Class<MenuItemOption> get() = MenuItemOption::class.java

    /**
     * Endpoint is responsible for retrieving multiple menu item options
     * based on a given set of ids.
     */
    fun listByIds(request: ServerRequest): Mono<ServerResponse> = repository
        .findByIds(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID).split(","))
        .collectList().flatMap { menuOptions -> ResponseBuilder.okResponse(menuOptions) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}