package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored DiscountCode.
 */
@Document(collection = "discount_codes")
data class DiscountCode(
    @Id override val id: String? = null,
    val title: String,
    val discount: Double,
    val discountType: String,
    val spendEligibility: Double?,
    val enabled: Boolean,
    val code: String
) : Model<DiscountCode>

/**
 * Model represents a single partial DiscountCode.
 */
data class PartialDiscountCode(
    @Id override val id: String? = null,
    val title: String? = null,
    val discount: Double? = null,
    val discountType: String? = null,
    val spendEligibility: Double? = null,
    val enabled: Boolean? = null
) : Model<PartialDiscountCode>