package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import java.util.Date

/**
 * Model represents a single stored Order.
 */
@Document(collection = "orders")
data class Order(
    override val id: String? = null,
    val customerId: String? = null,
    val orderStatus: OrderStatus,
    val price: Double,
    val orderOptions: List<OrderOption>,
    val discountCode: String? = null,
    val deliveryDriver: String? = null,

    val customerEmail: String,
    val customerName: String,
    val customerContactNumber: String,

    val street: String,
    val city: String,
    val county: String,
    val postcode1: String,
    val postcode2: String
) : Model<Order>

/**
 * Model represents a single inbound Order.
 */
data class InOrder(
    override val id: String? = null,
    val customerId: String? = null,
    val orderStatus: OrderStatus? = null,
    val orderOptions: List<OrderOption> = emptyList(),
    val discountCode: String? = null,
    val deliveryDriver: String?,

    val customerEmail: String? = null,
    val customerName: String? = null,
    val customerContactNumber: String? = null,

    val cardType: String? = null,
    val cardNumber: String? = null,
    val cardExpiry: Date? = null,
    val nameOnCard: String? = null,

    val street: String? = null,
    val city: String? = null,
    val county: String? = null,
    val postcode1: String? = null,
    val postcode2: String? = null
) : Model<InOrder>

/**
 * Model represents a set of inbound order options.
 */
data class OrderOption(
    val menuItemId: String? = null,
    val selectedOptionIds: List<String> = emptyList()
)

/**
 * Model represents the current status of an order.
 */
enum class OrderStatus {
    ACCEPTED, COOKING, DELIVERY, COMPLETED
}