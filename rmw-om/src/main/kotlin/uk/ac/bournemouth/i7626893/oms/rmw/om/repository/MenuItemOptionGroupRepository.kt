package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOptionGroup

/**
 * AbstractRepository class is responsible for providing the necessary
 * CRUD functionality for the menu_item_option_groups collection.
 */
class MenuItemOptionGroupRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<MenuItemOptionGroup>(template) {

    /**
     * Function is responsible for retrieving all menu item option
     * groups with the provided ids.
     */
    fun findByIds(ids: List<String>): Flux<MenuItemOptionGroup> = template
        .find(BasicQuery("{\"id\": {\"\$in\": ${ids.map { "\"$it\"" }}}}"), MenuItemOptionGroup::class.java)
}