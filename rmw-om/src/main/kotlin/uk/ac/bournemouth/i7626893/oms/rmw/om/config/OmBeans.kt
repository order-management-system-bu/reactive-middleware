package uk.ac.bournemouth.i7626893.oms.rmw.om.config

import com.mongodb.ConnectionString
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory
import uk.ac.bournemouth.i7626893.oms.rmw.core.CoreModule
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.DeliveryOptionHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.DiscountCodeHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuCategoryHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemOptionGroupHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemOptionHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.OrderHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.DeliveryOptionRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.DiscountCodeRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuCategoryRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemOptionGroupRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemOptionRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.OrderRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.DeliveryOptionValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.DiscountCodeValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuCategoryValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemOptionGroupValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemOptionValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.OrderValidator

object OmBeans {
    private val DB_CONNECTION_STRING: String =
        if (CoreModule.DEBUG_MODE) "mongodb://localhost:27017/oms-om-dev"
        else "mongodb://oms-rmw:aaJ&x##?@_w52_cPH%!EgARF^@ds055875.mlab.com:55875/oms-om"

    fun beans(): BeanDefinitionDsl = org.springframework.context.support.beans {
        // AbstractRepository beans.
        bean("om.DatabaseTemplate") { ReactiveMongoTemplate(SimpleReactiveMongoDatabaseFactory(ConnectionString(DB_CONNECTION_STRING))) }
        bean { MenuCategoryRepository(ref("om.DatabaseTemplate")) }
        bean { MenuItemRepository(ref("om.DatabaseTemplate")) }
        bean { MenuItemOptionRepository(ref("om.DatabaseTemplate")) }
        bean { MenuItemOptionGroupRepository(ref("om.DatabaseTemplate")) }
        bean { OrderRepository(ref("om.DatabaseTemplate")) }
        bean { DiscountCodeRepository(ref("om.DatabaseTemplate")) }
        bean { DeliveryOptionRepository(ref("om.DatabaseTemplate")) }

        // Validator beans.
        bean(name = "om.validator.menucategory.create") { MenuCategoryValidator(true) }
        bean(name = "om.validator.menucategory.update") { MenuCategoryValidator(false) }
        bean(name = "om.validator.menuitem.create") { MenuItemValidator(true) }
        bean(name = "om.validator.menuitem.update") { MenuItemValidator(false) }
        bean(name = "om.validator.menuitemoption.create") { MenuItemOptionValidator(true) }
        bean(name = "om.validator.menuitemoption.update") { MenuItemOptionValidator(false) }
        bean(name = "om.validator.menuitemoptiongroup.create") { MenuItemOptionGroupValidator(true) }
        bean(name = "om.validator.menuitemoptiongroup.update") { MenuItemOptionGroupValidator(false) }
        bean(name = "om.validator.order.create") { OrderValidator(true) }
        bean(name = "om.validator.order.update") { OrderValidator(false) }
        bean(name = "om.validator.discountcode.create") { DiscountCodeValidator(true) }
        bean(name = "om.validator.deliveryoption.create") { DeliveryOptionValidator(true) }
        bean(name = "om.validator.deliveryoption.update") { DeliveryOptionValidator(false) }

        // Handler beans.
        bean { MenuCategoryHandler(ref(), ref("om.validator.menucategory.create"), ref("om.validator.menucategory.update"), ref(), ref()) }
        bean { MenuItemHandler(ref(), ref("om.validator.menuitem.create"), ref("om.validator.menuitem.update")) }
        bean { MenuItemOptionHandler(ref(), ref("om.validator.menuitemoption.create"), ref("om.validator.menuitemoption.update")) }
        bean { MenuItemOptionGroupHandler(ref(), ref("om.validator.menuitemoptiongroup.create"), ref("om.validator.menuitemoptiongroup.update")) }
        bean { OrderHandler(ref(), ref("om.validator.order.create"), ref(), ref(), ref()) }
        bean { DiscountCodeHandler(ref(), ref("om.validator.discountcode.create"), ref(), ref(), ref()) }
        bean { DeliveryOptionHandler(ref(), ref("om.validator.deliveryoption.create"), ref("om.validator.deliveryoption.update")) }

        // Router beans.
        bean { OmRoutes.router(ref(), ref(), ref(), ref(), ref(), ref(), ref()) }
    }
}