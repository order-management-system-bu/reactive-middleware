package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureStore
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItem
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemValidator

/**
 * Handler class is responsible for exposing MenuItem-related endpoints
 * to external entities.
 */
class MenuItemHandler(
    override val repository: MenuItemRepository,
    override val cValidator: MenuItemValidator,
    override val uValidator: MenuItemValidator
) :
    SecureStore<MenuItem>,
    SecureUpdate<MenuItem>,
    SecureDelete<MenuItem>,
    UnsecureView<MenuItem> {

    override val permission: String get() = "om.menuitem"
    override val mClass: Class<MenuItem> get() = MenuItem::class.java

    /**
     * Endpoint is responsible for retrieving all MenuItems associated
     * with the provided MenuCategory.
     */
    fun listByMenuCategory(request: ServerRequest): Mono<ServerResponse> = repository
        .listByMenuCategory(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID))
        .collectList()
        .flatMap { ResponseBuilder.okResponse(it) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))

    /**
     * Endpoint is responsible for retrieving all MenuItems with the
     * provided IDs.
     */
    fun listByMenuItem(request: ServerRequest): Mono<ServerResponse> = repository
        .listByIds(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID).split(","))
        .collectList()
        .flatMap { ResponseBuilder.okResponse(it) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))

    /**
     * Endpoint is responsible for removing all menu items with
     * the provided menu category ID.
     */
    fun deleteByMenuCategory(request: ServerRequest): Mono<ServerResponse> = repository
        .listByMenuCategory(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID))
        .flatMap { repository.delete(it) }
        .collectList()
        .flatMap { ResponseBuilder.okResponse(Unit) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}