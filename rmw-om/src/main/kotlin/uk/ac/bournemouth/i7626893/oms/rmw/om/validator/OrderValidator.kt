package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import org.apache.commons.validator.routines.EmailValidator
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.LuhnValidator
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.InOrder

/**
 * Validator class is responsible for validating inbound menu items.
 */
class OrderValidator(validateMandatoryFields: Boolean) : AbstractValidator<InOrder>(validateMandatoryFields) {
    override fun validateFields(m: InOrder): List<ValidationFault> = listOfNotNull(
        ValidationFault("orderOptions", "One or more order options where not valid.")
            .takeIf {
                m.orderOptions.isNotEmpty() && m.orderOptions.any { menuItem ->
                    !menuItem.menuItemId.isNullOrBlank() && !menuItem.menuItemId!!.matches(ValidationRegex.OBJECT_ID)
                }
            },
        ValidationFaults.invalidCharacters("customerId")
            .takeIf { m.customerId != null && !m.customerId.matches(ValidationRegex.OBJECT_ID) },
        ValidationFault("customerEmail", ERROR_INVALID_EMAIL)
            .takeIf { m.customerEmail != null && !EmailValidator.getInstance(false).isValid(m.customerEmail) },
        ValidationFaults.invalidCharacters("customerName")
            .takeIf { m.customerName != null && !m.customerName.matches(ValidationRegex.ALPHA_W_SPACE) },
        ValidationFaults.exceededMaxLength("customerName", 60)
            .takeIf { m.customerName != null && m.customerName.length > 60 },
        ValidationFault("contactNumber", ERROR_INVALID_PHONE_NUMBER)
            .takeIf { !m.customerContactNumber.isNullOrBlank() && !m.customerContactNumber!!.matches(ValidationRegex.MOBILE_NUMBER) },
        ValidationFaults.exceededMaxLength("nameOnCard", 50)
            .takeIf { m.nameOnCard != null && m.nameOnCard.length > 50 },
        ValidationFault("cardNumber", ERROR_INVALID_CARD_NUMBER)
            .takeIf { !m.cardNumber.isNullOrBlank() && !LuhnValidator.Check(m.cardNumber!!) },
        ValidationFault("cardType", "The provided card type was unrecognised")
            .takeIf { !m.cardType.isNullOrBlank() && !listOf("VISA", "Mastercard", "American Express").contains(m.cardType!!) },
        ValidationFaults.invalidCharacters("street")
            .takeIf { m.street != null && !m.street.matches(ValidationRegex.ALPHANUMERIC_W_SPACE) },
        ValidationFaults.invalidCharacters("city")
            .takeIf { m.city != null && !m.city.matches(ValidationRegex.ALPHA_W_SPACE) },
        ValidationFaults.invalidCharacters("county")
            .takeIf { m.county != null && !m.county.matches(ValidationRegex.ALPHA_W_SPACE) },
        ValidationFaults.invalidCharacters("postcode1")
            .takeIf { m.postcode1 != null && !m.postcode1.matches(ValidationRegex.ALPHANUMERIC) },
        ValidationFaults.invalidCharacters("postcode2")
            .takeIf { m.postcode2 != null && !m.postcode2.matches(ValidationRegex.ALPHANUMERIC) },
        ValidationFaults.exceededMaxLength("postcode1", 4)
            .takeIf { m.postcode1 != null && m.postcode1.length > 4 },
        ValidationFaults.exceededMaxLength("postcode2", 4)
            .takeIf { m.postcode2 != null && m.postcode2.length > 4 }
    )

    override fun validateMandatoryFields(m: InOrder): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("orderOptions").takeIf { m.orderOptions.isEmpty() },
        ValidationFaults.missingField("orderOptions").takeIf { m.orderOptions.any { menuItem -> menuItem.menuItemId.isNullOrBlank() } },
        ValidationFaults.missingField("customerEmail").takeIf { m.customerEmail.isNullOrBlank() },
        ValidationFaults.missingField("customerName").takeIf { m.customerName.isNullOrBlank() },
        ValidationFaults.missingField("customerContactNumber").takeIf { m.customerContactNumber.isNullOrBlank() },
        ValidationFaults.missingField("cardType").takeIf { m.cardType.isNullOrBlank() },
        ValidationFaults.missingField("cardNumber").takeIf { m.cardNumber.isNullOrBlank() },
        ValidationFaults.missingField("cardExpiry").takeIf { m.cardExpiry == null },
        ValidationFaults.missingField("nameOnCard").takeIf { m.nameOnCard.isNullOrBlank() },

        ValidationFaults.missingField("street").takeIf { m.street.isNullOrBlank() },
        ValidationFaults.missingField("city").takeIf { m.city.isNullOrBlank() },
        ValidationFaults.missingField("county").takeIf { m.county.isNullOrBlank() },
        ValidationFaults.missingField("postcode1").takeIf { m.postcode1.isNullOrBlank() },
        ValidationFaults.missingField("postcode2").takeIf { m.postcode2.isNullOrBlank() }
    )

    companion object {
        private const val ERROR_INVALID_EMAIL: String = "The provided email address was not valid."
        private const val ERROR_INVALID_CARD_NUMBER: String = "The provided card number was not valid."
        private const val ERROR_INVALID_PHONE_NUMBER: String = "The provided contact number was not a valid mobile number."
    }
}