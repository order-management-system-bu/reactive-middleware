package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureStore
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOptionGroup
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuItemOptionGroupRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuItemOptionGroupValidator

/**
 * Handler class is responsible for exposing MenuItemOptionGroup-related
 * endpoints to external entities.
 */
class MenuItemOptionGroupHandler(
    override val repository: MenuItemOptionGroupRepository,
    override val cValidator: MenuItemOptionGroupValidator,
    override val uValidator: MenuItemOptionGroupValidator
) :
    SecureStore<MenuItemOptionGroup>,
    SecureUpdate<MenuItemOptionGroup>,
    SecureDelete<MenuItemOptionGroup>,
    UnsecureView<MenuItemOptionGroup>,
    UnsecureList<MenuItemOptionGroup> {

    override val permission: String get() = "om.menuitemoptiongroup"
    override val mClass: Class<MenuItemOptionGroup> get() = MenuItemOptionGroup::class.java

    fun listByIds(request: ServerRequest): Mono<ServerResponse> = repository
        .findByIds(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID).split(","))
        .collectList().flatMap { menuOptionGroups -> ResponseBuilder.okResponse(menuOptionGroups) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}