package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuCategory

/**
 * Validator class is responsible for validating inbound menu categories.
 */
class MenuCategoryValidator(validateMandatoryFields: Boolean) : AbstractValidator<MenuCategory>(validateMandatoryFields) {
    override fun validateFields(m: MenuCategory): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("title")
            .takeIf { m.title != null && !m.title.matches(ValidationRegex.SENTENCE) },
        ValidationFaults.invalidCharacters("description")
            .takeIf { m.description != null && !m.description.matches(ValidationRegex.SENTENCE) },
        ValidationFaults.exceededMaxLength("title", 30)
            .takeIf { m.title != null && m.title.length > 30 },
        ValidationFaults.exceededMaxLength("description", 500)
            .takeIf { m.description != null && m.description.length > 500 }
    )

    override fun validateMandatoryFields(m: MenuCategory): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("title").takeIf { m.title.isNullOrBlank() }
    )
}