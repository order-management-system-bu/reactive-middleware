package uk.ac.bournemouth.i7626893.oms.rmw.om.config

import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.DeliveryOptionHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.DiscountCodeHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuCategoryHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemOptionGroupHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.MenuItemOptionHandler
import uk.ac.bournemouth.i7626893.oms.rmw.om.handler.OrderHandler

/**
 * Object is responsible for providing the OM modules
 * routing schema.
 */
object OmRoutes {
    fun router(
        menuCategoryHandler: MenuCategoryHandler,
        menuItemHandler: MenuItemHandler,
        menuItemOptionHandler: MenuItemOptionHandler,
        menuItemOptionGroupHandler: MenuItemOptionGroupHandler,
        orderHandler: OrderHandler,
        discountCodeHandler: DiscountCodeHandler,
        deliveryOptionHandler: DeliveryOptionHandler
    ): RouterFunction<ServerResponse> = org.springframework.web.reactive.function.server.router {
        "/oms/rmw/om".nest {
            "/menu-category".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", menuCategoryHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", menuCategoryHandler::update)
                }
                GET("/list", menuCategoryHandler::list)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", menuCategoryHandler::view)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", menuCategoryHandler::delete)
            }

            "/menu-item".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", menuItemHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", menuItemHandler::update)
                }
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", menuItemHandler::view)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/list/menu_category", menuItemHandler::listByMenuCategory)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}:[a-f\\d]{24}+(?:,[a-f\\d]{24}+)*}/list", menuItemHandler::listByMenuItem)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", menuItemHandler::delete)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete/category", menuItemHandler::deleteByMenuCategory)
            }

            "/menu-item-option".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", menuItemOptionHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", menuItemOptionHandler::update)
                }
                GET("/list", menuItemOptionHandler::list)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}:[a-f\\d]{24}+(?:,[a-f\\d]{24}+)*}/list", menuItemOptionHandler::listByIds)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", menuItemOptionHandler::view)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", menuItemOptionHandler::delete)
            }

            "/menu-item-option-group".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", menuItemOptionGroupHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", menuItemOptionGroupHandler::update)
                }
                GET("/list", menuItemOptionGroupHandler::list)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}:[a-f\\d]{24}+(?:,[a-f\\d]{24}+)*}/list", menuItemOptionGroupHandler::listByIds)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", menuItemOptionGroupHandler::view)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", menuItemOptionGroupHandler::delete)
            }

            "/order".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", orderHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delivery-driver", orderHandler::assignDeliveryDriver)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/{status:\\bCOOKING|\\bDELIVERY|\\bCOMPLETED|\\bCANCELED}", orderHandler::updateOrderStatus)
                }
                GET("/list", orderHandler::list)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/list/delivery-driver", orderHandler::listOrdersByDeliveryDriver)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/list/customer", orderHandler::listOrdersByCustomer)
                GET("/list/{${HandlerConstants.PATH_VAR_ENTITY_ID}:[A-Z]+(?:,[A-Z]+)*}", orderHandler::listOrdersByStatus)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", orderHandler::view)
            }

            "/discount-code".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", discountCodeHandler::store)
                    POST("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/send", discountCodeHandler::sendDiscountCode)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", discountCodeHandler::update)
                }
                GET("/list", discountCodeHandler::list)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", discountCodeHandler::view)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view/code", discountCodeHandler::viewByDiscountCode)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", discountCodeHandler::delete)
            }

            "/delivery-option".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    PUT("", deliveryOptionHandler::update)
                }
                GET("/view", deliveryOptionHandler::view)
            }
        }
    }
}