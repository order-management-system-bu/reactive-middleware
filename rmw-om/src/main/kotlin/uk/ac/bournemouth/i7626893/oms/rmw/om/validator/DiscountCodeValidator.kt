package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.PartialDiscountCode

/**
 * Validator class is responsible for validating inbound discount codes.
 */
class DiscountCodeValidator(validateMandatoryFields: Boolean) : AbstractValidator<PartialDiscountCode>(validateMandatoryFields) {
    override fun validateFields(m: PartialDiscountCode): List<ValidationFault> = listOfNotNull(
        ValidationFaults.exceededMaxLength("title", 30)
            .takeIf { m.title != null && m.title.length > 30 },
        ValidationFault("discount", ERROR_UNKNOWN_DISCOUNT_TYPE)
            .takeIf { m.discountType != null && !listOf("PERCENTAGE", "FIXED").contains(m.discountType) },

        ValidationFault("discount", ERROR_LESS_THAN_0)
            .takeIf { m.discount != null && m.discount < 0 },
        ValidationFault("discount", ERROR_MORE_THAN_100)
            .takeIf { m.discount != null && m.discountType != null && m.discountType == "PERCENTAGE" && m.discount > 100 },
        ValidationFault("spendEligibility", ERROR_LESS_THAN_0)
            .takeIf { m.spendEligibility != null && m.spendEligibility < 0 }
    )

    override fun validateMandatoryFields(m: PartialDiscountCode): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("title").takeIf { m.title.isNullOrBlank() },
        ValidationFaults.missingField("discount").takeIf { m.discount == null },
        ValidationFaults.missingField("discountType").takeIf { m.discountType.isNullOrBlank() }
    )

    companion object {
        private const val ERROR_UNKNOWN_DISCOUNT_TYPE: String =
            "The provided discount type was not recognised."
        private const val ERROR_LESS_THAN_0: String =
            "This field must have a value of 0 or greater."
        private const val ERROR_MORE_THAN_100: String =
            "This field must have a value no greater than 100."
    }
}