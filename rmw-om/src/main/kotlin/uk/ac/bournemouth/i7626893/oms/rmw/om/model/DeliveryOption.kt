package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a restauraunts delivery options.
 */
@Document(collection = "delivery_options")
data class DeliveryOption(
    override val id: String? = null,
    val deliveryCharge: Double? = null,
    val minimumForDelivery: Double? = null
) : Model<DeliveryOption>