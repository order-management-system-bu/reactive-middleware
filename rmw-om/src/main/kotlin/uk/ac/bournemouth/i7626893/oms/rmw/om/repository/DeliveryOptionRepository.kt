package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DeliveryOption

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the deliver_options collection.
 */
class DeliveryOptionRepository(template: ReactiveMongoTemplate) : AbstractRepository<DeliveryOption>(template)