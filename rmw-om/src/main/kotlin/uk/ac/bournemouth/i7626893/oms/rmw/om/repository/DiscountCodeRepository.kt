package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DiscountCode

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the menu_categories collection.
 */
class DiscountCodeRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<DiscountCode>(template) {

    /**
     * Function is responsible for retrieving a single discount code
     * based on its discount code.
     */
    fun findByDiscountCode(discountCode: String): Mono<DiscountCode> = template
        .findOne(BasicQuery("{\"code\": \"$discountCode\"}"), DiscountCode::class.java)
}