package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored MenuItem.
 */
@Document(collection = "menu_items")
data class MenuItem(
    @Id override val id: String? = null,
    val menuCategoryId: String? = null,
    val title: String? = null,
    val description: String? = null,
    val price: Double? = null,
    val additionalOptionIds: List<String> = emptyList(),
    val additionalOptionGroupIds: List<String> = emptyList()
) : Model<MenuItem> {
    override fun update(m: MenuItem): MenuItem = m.copy(
        title = title.takeIf { !it.isNullOrBlank() } ?: m.title,
        description = title.takeIf { !it.isNullOrBlank() } ?: m.description,
        price = price.takeIf { it != null } ?: m.price,
        additionalOptionIds = additionalOptionIds.takeIf { it.isNotEmpty() } ?: m.additionalOptionIds,
        additionalOptionGroupIds = additionalOptionGroupIds.takeIf { it.isNotEmpty() } ?: m.additionalOptionGroupIds
    )
}