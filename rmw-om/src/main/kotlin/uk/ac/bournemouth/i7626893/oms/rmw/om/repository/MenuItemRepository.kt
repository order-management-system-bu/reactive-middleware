package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItem

/**
 * AbstractRepository class is responsible for providing the necessary
 * CRUD functionality for the menu_items collection.
 */
class MenuItemRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<MenuItem>(template) {

    /**
     * Function is responsible for retrieving all MenuItem instances
     * based on the provided IDs.
     */
    fun listByIds(ids: List<String>): Flux<MenuItem> = template
        .find(BasicQuery("{\"id\": {\"\$in\": ${ids.map { "\"$it\"" }}}}"), MenuItem::class.java)

    /**
     * Function is responsible for retrieving all MenuItems associated
     * with the provided MenuCategory.
     */
    fun listByMenuCategory(menuCategoryId: String): Flux<MenuItem> = template
        .find(BasicQuery("{\"menuCategoryId\": \"$menuCategoryId\"}"), MenuItem::class.java)
}