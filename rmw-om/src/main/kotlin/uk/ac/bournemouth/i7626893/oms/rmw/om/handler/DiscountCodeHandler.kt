package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.toEntity
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications.DiscountCodeNotification
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ENotificationsEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.URMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DiscountCode
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.PartialDiscountCode
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.DiscountCodeRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.DiscountCodeValidator
import java.util.Random

/**
 * Handler class is responsible for exposing DiscountCode-related endpoints
 * to external entities.
 */
class DiscountCodeHandler(
    override val repository: DiscountCodeRepository,
    private val cValidator: DiscountCodeValidator,
    private val eNotificationsEndpoints: ENotificationsEndpoints,
    private val URMEndpoints: URMEndpoints,
    private val webClient: WebClient
) :
    SecureDelete<DiscountCode>,
    SecureView<DiscountCode>,
    SecureList<DiscountCode> {

    override val permission: String get() = "om.discountcode"
    override val mClass: Class<DiscountCode> get() = DiscountCode::class.java

    /**
     * Endpoint is responsible for allowing external entities to
     * store discount codes in the applications database.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.discountcode.store") {
            request.bodyToMono(PartialDiscountCode::class.java)
                .flatMap { partialDiscountCode ->
                    val validationFaults = cValidator.validate(partialDiscountCode)
                    if (validationFaults.isNotEmpty()) {
                        ResponseBuilder.badRequestResponse(validationFaults)
                    } else {
                        val charPool = ('A'..'Z') + ('0'..'9')
                        repository
                            .store(DiscountCode(
                                title = partialDiscountCode.title!!,
                                discount = partialDiscountCode.discount!!,
                                discountType = partialDiscountCode.discountType!!,
                                spendEligibility = partialDiscountCode.spendEligibility,
                                enabled = partialDiscountCode.enabled!!,
                                code = (1..8).map { charPool[Random().nextInt(charPool.size)] }.joinToString("")
                            ))
                            .flatMap { ResponseBuilder.createdResponse(it.id!!) }
                    }
                }
        }

    /**
     * Endpoint is responsible for updating an existing
     * discount code.
     */
    fun update(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.discountcode.update") {
            request.bodyToMono(PartialDiscountCode::class.java).flatMap { partialDiscountCode ->
                repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), DiscountCode::class.java).flatMap { discountCode ->
                    repository.store(discountCode.copy(
                        enabled = partialDiscountCode.enabled.takeIf { it != null } ?: discountCode.enabled)
                    ).flatMap { ResponseBuilder.okResponse(Unit) }
                }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
            }
        }

    /**
     * Endpoint is responsible for retrieving a single discount code
     * based on its code.
     */
    fun viewByDiscountCode(request: ServerRequest): Mono<ServerResponse> = repository
        .findByDiscountCode(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID)).flatMap { discountCode ->
            ResponseBuilder.okResponse(discountCode)
        }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))

    /**
     * Endpoint is responsible allowing external entities to
     * send discount code notifications.
     */
    fun sendDiscountCode(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.discountcode.list") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), DiscountCode::class.java)
                .flatMap { discountCode ->
                    webClient.get().uri(URMEndpoints.listContactableCustomerEmails())
                        .header("Authorization", request.headers().header("Authorization").first())
                        .exchange()
                        .flatMap { clientResponse ->
                            when (clientResponse.statusCode()) {
                                HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)
                                HttpStatus.OK -> clientResponse.toEntity<List<String>>().flatMap { response ->
                                    webClient.post().uri(eNotificationsEndpoints.createDiscountCodeEndpoint())
                                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                                        .body(BodyInserters.fromObject(DiscountCodeNotification(
                                            target_email = response.body!!,
                                            discount_title = discountCode.title,
                                            discount_code = discountCode.code
                                        )))
                                        .exchange()
                                        .flatMap { notificationsResponse ->
                                            when (notificationsResponse.statusCode()) {
                                                HttpStatus.CREATED -> ResponseBuilder.okResponse(Unit)
                                                else -> ResponseBuilder.internalErrorResponse(Unit)
                                            }
                                        }
                                }
                                else -> ResponseBuilder.internalErrorResponse(Unit)
                            }
                        }
                }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }
}