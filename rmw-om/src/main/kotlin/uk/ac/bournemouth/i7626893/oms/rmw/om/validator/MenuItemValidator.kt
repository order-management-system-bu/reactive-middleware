package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItem

/**
 * Validator class is responsible for validating inbound menu items.
 */
class MenuItemValidator(validateMandatoryFields: Boolean) : AbstractValidator<MenuItem>(validateMandatoryFields) {
    override fun validateFields(m: MenuItem): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("title")
            .takeIf { m.title != null && !m.title.matches(ValidationRegex.SENTENCE) },
        ValidationFaults.invalidCharacters("additionalOptionIds")
            .takeIf { m.additionalOptionIds.isNotEmpty() && m.additionalOptionIds.any { id -> !id.matches(ValidationRegex.OBJECT_ID) } },
        ValidationFaults.invalidCharacters("additionalOptionGroupIds")
            .takeIf { m.additionalOptionGroupIds.isNotEmpty() && m.additionalOptionGroupIds.any { id -> !id.matches(ValidationRegex.OBJECT_ID) } },
        ValidationFaults.exceededMaxLength("title", 30)
            .takeIf { m.title != null && m.title.length > 30 }
    )

    override fun validateMandatoryFields(m: MenuItem): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("menuCategoryId").takeIf { m.menuCategoryId.isNullOrBlank() },
        ValidationFaults.missingField("title").takeIf { m.title.isNullOrBlank() },
        ValidationFaults.missingField("price").takeIf { m.price == null }
    )
}