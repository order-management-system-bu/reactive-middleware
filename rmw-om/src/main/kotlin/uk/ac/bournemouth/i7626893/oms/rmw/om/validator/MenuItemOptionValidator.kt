package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOption

/**
 * Validator class is responsible for validating inbound menu item options.
 */
class MenuItemOptionValidator(validateMandatoryFields: Boolean) : AbstractValidator<MenuItemOption>(validateMandatoryFields) {
    override fun validateFields(m: MenuItemOption): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("title")
            .takeIf { m.title != null && !m.title.matches(ValidationRegex.SENTENCE) },
        ValidationFaults.exceededMaxLength("title", 30)
            .takeIf { m.title != null && m.title.length > 30 }
    )

    override fun validateMandatoryFields(m: MenuItemOption): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("title").takeIf { m.title.isNullOrBlank() },
        ValidationFaults.missingField("price").takeIf { m.price == null }
    )
}