package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DeliveryOption
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.DeliveryOptionRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.DeliveryOptionValidator

/**
 * Handler class is responsible for exposing DeliveryOption-related endpoints
 * to external entities.
 */
class DeliveryOptionHandler(
    private val repository: DeliveryOptionRepository,
    private val cValidator: DeliveryOptionValidator,
    private val uValidator: DeliveryOptionValidator
) {

    /**
     * Endpoint is responsible for updating delivery options.
     */
    fun update(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.deliveryoption.update") {
            request.bodyToMono(DeliveryOption::class.java).flatMap { deliveryOption ->
                repository.list(DeliveryOption::class.java)
                    .collectList().flatMap {
                        if (it.isNotEmpty()) {
                            val validationFaults = uValidator.validate(deliveryOption)
                            if (validationFaults.isNotEmpty())
                                ResponseBuilder.badRequestResponse(validationFaults)
                            else repository.store(it.first().copy(
                                deliveryCharge = deliveryOption.deliveryCharge.takeIf { dc -> dc != null }
                                    ?: it.first().deliveryCharge,
                                minimumForDelivery = deliveryOption.minimumForDelivery.takeIf { mfd -> mfd != null }
                                    ?: it.first().minimumForDelivery
                            )).flatMap { ResponseBuilder.okResponse(Unit) }
                        } else {
                            val validationFaults = cValidator.validate(deliveryOption)
                            if (validationFaults.isNotEmpty())
                                ResponseBuilder.badRequestResponse(validationFaults)
                            else repository.store(deliveryOption).flatMap { ResponseBuilder.okResponse(Unit) }
                        }
                    }
            }
        }

    /**
     * Endpoint is responsible for retrieving delivery options
     * from the applications database.
     */
    fun view(request: ServerRequest): Mono<ServerResponse> = repository
        .list(DeliveryOption::class.java)
        .collectList()
        .flatMap { ResponseBuilder.okResponse(it.first()) }
        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
}