package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuItemOptionGroup

/**
 * Validator class is responsible for validating inbound menu item option groups.
 */
class MenuItemOptionGroupValidator(
    validateMandatoryFields: Boolean
) : AbstractValidator<MenuItemOptionGroup>(validateMandatoryFields) {
    override fun validateFields(m: MenuItemOptionGroup): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("title")
            .takeIf { m.title != null && !m.title.matches(ValidationRegex.SENTENCE) },
        ValidationFaults.invalidCharacters("menuItemOptions")
            .takeIf { m.menuItemOptions.any { id -> !id.matches(ValidationRegex.OBJECT_ID) } },
        ValidationFaults.exceededMaxLength("title", 30)
            .takeIf { m.title != null && m.title.length > 30 }
    )

    override fun validateMandatoryFields(m: MenuItemOptionGroup): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("title").takeIf { m.title.isNullOrBlank() },
        ValidationFaults.missingField("menuItemOptions").takeIf { m.menuItemOptions.isEmpty() }
    )
}