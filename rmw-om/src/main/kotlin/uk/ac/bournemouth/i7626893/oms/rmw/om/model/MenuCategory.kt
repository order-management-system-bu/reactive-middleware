package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored MenuCategory.
 */
@Document(collection = "menu_categories")
data class MenuCategory(
    @Id override val id: String? = null,
    val title: String? = null,
    val description: String? = null
) : Model<MenuCategory> {
    override fun update(m: MenuCategory): MenuCategory = m.copy(
        title = title.takeIf { !title.isNullOrBlank() } ?: m.title,
        description = description.takeIf { !description.isNullOrBlank() } ?: m.description
    )
}