package uk.ac.bournemouth.i7626893.oms.rmw.om.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored MenuItemOptionGroup.
 */
@Document(collection = "menu_item_option_groups")
data class MenuItemOptionGroup(
    @Id override val id: String? = null,
    val title: String? = null,
    val menuItemOptions: List<String> = emptyList()
) : Model<MenuItemOptionGroup> {
    override fun update(m: MenuItemOptionGroup): MenuItemOptionGroup = m.copy(
        title = title.takeIf { !it.isNullOrBlank() } ?: m.title,
        menuItemOptions = menuItemOptions.takeIf { it.isNotEmpty() } ?: m.menuItemOptions
    )
}