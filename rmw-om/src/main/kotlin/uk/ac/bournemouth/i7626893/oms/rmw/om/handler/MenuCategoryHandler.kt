package uk.ac.bournemouth.i7626893.oms.rmw.om.handler

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureStore
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureList
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.unsecure.UnsecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.OMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuCategory
import uk.ac.bournemouth.i7626893.oms.rmw.om.repository.MenuCategoryRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.validator.MenuCategoryValidator

/**
 * Handler class is responsible for exposing MenuCategory-related endpoints
 * to external entities.
 */
class MenuCategoryHandler(
    override val repository: MenuCategoryRepository,
    override val cValidator: MenuCategoryValidator,
    override val uValidator: MenuCategoryValidator,
    private val omEndpoints: OMEndpoints,
    private val webClient: WebClient
) :
    SecureStore<MenuCategory>,
    SecureUpdate<MenuCategory>,
    UnsecureView<MenuCategory>,
    UnsecureList<MenuCategory> {

    override val permission: String get() = "om.menucategory"
    override val mClass: Class<MenuCategory> get() = MenuCategory::class.java

    /**
     * Function is responsible for removing a single MenuCategory
     * and all associaged MenuItems.
     */
    fun delete(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.om.menucategory.delete") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), MenuCategory::class.java).flatMap { menuCategory ->
                webClient.delete().uri(omEndpoints.removeMenuItemByCategory(menuCategory.id!!))
                    .header("Authorization", request.headers().header("Authorization").first())
                    .exchange()
                    .flatMap { clientResponse ->
                        when (clientResponse.statusCode()) {
                            HttpStatus.OK -> repository.delete(menuCategory).flatMap { ResponseBuilder.okResponse(Unit) }

                            HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)

                            else -> ResponseBuilder.internalErrorResponse(Unit)
                        }
                    }
            }
        }
}