package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.Order

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the menu_items collection.
 */
class OrderRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<Order>(template) {

    /**
     * Function is responsible for retrieving all stored Orders
     * based on the provided OrderStatuses.
     */
    fun listByOrderStatuses(statuses: List<String>): Flux<Order> = template
        .find(BasicQuery("{\"orderStatus\": {\"\$in\": ${statuses.map { "\"$it\"" }}}}"), Order::class.java)

    /**
     * Function is responsible for retrieving all stored orders
     * that are associated with the provided delivery driver.
     */
    fun listByDeliveryDriver(deliveryDriverId: String): Flux<Order> = template
        .find(BasicQuery("{\"deliveryDriver\": \"$deliveryDriverId\"}"), Order::class.java)

    /**
     * Function is responsible for retrieving all stored orders
     * that are associated with the provided customer.
     */
    fun listByCustomer(customerId: String): Flux<Order> = template
        .find(BasicQuery("{\"customerId\": \"$customerId\"}"), Order::class.java)
}