package uk.ac.bournemouth.i7626893.oms.rmw.om.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.DeliveryOption

/**
 * Validator class is responsible for validating inbound delivery options.
 */
class DeliveryOptionValidator(validateMandatoryFields: Boolean) : AbstractValidator<DeliveryOption>(validateMandatoryFields) {
    override fun validateFields(m: DeliveryOption): List<ValidationFault> = listOfNotNull(
        ValidationFault("deliveryCharge", ERROR_LESS_THAN_0)
            .takeIf { m.deliveryCharge != null && m.deliveryCharge < 0 },
        ValidationFault("minimumForDelivery", ERROR_LESS_THAN_0)
            .takeIf { m.minimumForDelivery != null && m.minimumForDelivery < 0 }
    )

    override fun validateMandatoryFields(m: DeliveryOption): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("deliveryCharge").takeIf { m.deliveryCharge == null },
        ValidationFaults.missingField("minimumForDelivery").takeIf { m.minimumForDelivery == null }
    )

    companion object {
        private const val ERROR_LESS_THAN_0: String =
            "This field must have a value of 0 or greater."
    }
}