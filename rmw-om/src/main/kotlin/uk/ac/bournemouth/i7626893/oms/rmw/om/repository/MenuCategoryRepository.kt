package uk.ac.bournemouth.i7626893.oms.rmw.om.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.om.model.MenuCategory

/**
 * AbstractRepository class is responsible for providing the necessary
 * CRUD functionality for the menu_categories collection.
 */
class MenuCategoryRepository(template: ReactiveMongoTemplate) : AbstractRepository<MenuCategory>(template)