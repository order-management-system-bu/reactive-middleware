#!/usr/bin/env bash
mvn -U clean install

cd rmw-app/
mvn spring-boot:run