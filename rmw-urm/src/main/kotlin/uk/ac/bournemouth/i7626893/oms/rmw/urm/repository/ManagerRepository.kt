package uk.ac.bournemouth.i7626893.oms.rmw.urm.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.Manager

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the managers collection.
 */
class ManagerRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<Manager>(template) {

    /**
     * Function is responsible for retrieving a single manager
     * from the applications database based on its user ID.
     */
    fun findByUserId(userId: String): Mono<Manager> = template
        .findOne(BasicQuery("{\"userId\": \"$userId\"}"), Manager::class.java)
}