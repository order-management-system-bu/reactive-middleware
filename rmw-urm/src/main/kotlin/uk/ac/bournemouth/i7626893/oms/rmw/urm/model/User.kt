package uk.ac.bournemouth.i7626893.oms.rmw.urm.model

import at.favre.lib.crypto.bcrypt.BCrypt
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import java.util.Date

/**
 * Model represents a single stored user.
 */
@Document(collection = "users")
data class User(
    override val id: String? = null,
    val type: String? = null,
    val username: String? = null,
    val password: String? = null,
    val forename: String? = null,
    val surname: String? = null,
    val active: Boolean? = null,
    val admin: Boolean? = null,
    val activationDate: Date? = null,
    val permissions: List<String>? = null
) : Model<User> {
    override fun update(m: User): User = m.copy(
        password = password?.let { BCrypt.withDefaults().hashToString(8, password.toCharArray()) } ?: m.password,
        forename = forename.takeIf { !it.isNullOrBlank() } ?: m.forename,
        surname = surname.takeIf { !it.isNullOrBlank() } ?: m.surname
    )
}

/**
 * Model represents a single outbound user.
 */
data class OutUser(
    val id: String,
    val username: String,
    val forename: String?,
    val surname: String?,
    val active: Boolean,
    val admin: Boolean,
    val activationDate: Date?
)

/**
 * Model represents a single authentication user.
 */
data class AuthenticateUser(
    val username: String? = null,
    val password: String? = null
)