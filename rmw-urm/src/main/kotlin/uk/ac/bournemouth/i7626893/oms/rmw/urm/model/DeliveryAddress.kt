package uk.ac.bournemouth.i7626893.oms.rmw.urm.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored delivery address.
 */
@Document(collection = "addresses")
data class DeliveryAddress(
    @Id override val id: String? = null,
    val customerId: String? = null,
    val street: String? = null,
    val city: String? = null,
    val county: String? = null,
    val postcode1: String? = null,
    val postcode2: String? = null,
    val defaultAddress: Boolean? = null // Is this address the customers default?
) : Model<DeliveryAddress> {
    override fun update(m: DeliveryAddress): DeliveryAddress = m.copy(
        street = street.takeIf { !it.isNullOrBlank() } ?: m.street,
        city = city.takeIf { !it.isNullOrBlank() } ?: m.city,
        county = county.takeIf { !it.isNullOrBlank() } ?: m.county,
        postcode1 = postcode1.takeIf { !it.isNullOrBlank() } ?: m.postcode1,
        postcode2 = postcode2.takeIf { !it.isNullOrBlank() } ?: m.postcode2,
        defaultAddress = defaultAddress.takeIf { it != null } ?: m.defaultAddress
    )
}