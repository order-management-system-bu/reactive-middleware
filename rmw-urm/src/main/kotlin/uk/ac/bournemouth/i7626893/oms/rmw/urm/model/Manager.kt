package uk.ac.bournemouth.i7626893.oms.rmw.urm.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a stored manager user associated with the
 * OMS-Manager application.
 */
@Document(collection = "managers")
data class Manager(
    @Id override val id: String? = null,
    val userId: String,
    val firstTimeLogin: Boolean,
    val deliveryDriver: Boolean
) : Model<Manager>

/**
 * Model represents a single inbound manager.
 */
data class InManager(
    @Id override val id: String? = null,
    val username: String? = null,
    val password: String? = null,
    val forename: String? = null,
    val surname: String? = null,
    val firstTimeLogin: Boolean? = null,
    val deliveryDriver: Boolean? = null
) : Model<InManager>

/**
 * Model represents a single outbound manager.
 */
data class OutManager(
    val id: String,
    val userId: String,
    val username: String,
    val forename: String?,
    val surname: String?,
    val firstTimeLogin: Boolean,
    val admin: Boolean,
    val active: Boolean,
    val deliveryDriver: Boolean
)