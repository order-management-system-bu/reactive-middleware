package uk.ac.bournemouth.i7626893.oms.rmw.urm.handler

import at.favre.lib.crypto.bcrypt.BCrypt
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications.ManagerCreationNotification
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications.UserActivationNotification
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ENotificationsEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.URMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.SecurityConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.AuthenticateUser
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.OutUser
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.User
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.UserRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.UserValidator
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.Date
import java.util.Random

/**
 * Handler class is responsible for exposing user
 * related endpoints to external entities.
 */
class UserHandler(
    override val repository: UserRepository,
    private val cValidator: UserValidator,
    override val uValidator: UserValidator,
    private val eNotificationsEndpoints: ENotificationsEndpoints,
    private val URMEndpoints: URMEndpoints,
    private val webClient: WebClient
) : SecureUpdate<User>, SecureDelete<User> {

    override val permission: String get() = "urm.user"
    override val mClass: Class<User> get() = User::class.java

    /**
     * Endpoint is responsible for creating a new user.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .bodyToMono(User::class.java).flatMap { user ->
            user.username?.let { username ->
                repository.findByUsername(username)
                    .flatMap { ResponseBuilder.badRequestResponse(listOf(ValidationFault("username", ERROR_USERNAME_IN_USE))) }
                    .switchIfEmpty {
                        when (user.type) {
                            "CUSTOMER" -> {
                                val validationFaults = cValidator.validate(user)
                                if (validationFaults.isNotEmpty())
                                    ResponseBuilder.badRequestResponse(validationFaults)
                                else repository.store(user.copy(
                                    password = BCrypt.withDefaults().hashToString(8, user.password!!.toCharArray()),
                                    active = false,
                                    admin = false,
                                    activationDate = null,
                                    permissions = USER_CUSTOMER_PERMISSIONS
                                )).flatMap { stored ->
                                    webClient.post().uri(eNotificationsEndpoints.createCustomerActivationEndpoint())
                                        .body(BodyInserters.fromObject(UserActivationNotification(
                                            target_email = stored.username!!,
                                            message = URMEndpoints.activateUser(stored.id!!)
                                        )))
                                        .exchange()
                                        .flatMap { clientResponse ->
                                            if (clientResponse.statusCode() == HttpStatus.CREATED)
                                                ResponseBuilder.createdResponse(stored.id)
                                            else repository.delete(stored)
                                                .flatMap { ResponseBuilder.internalErrorResponse(ERROR_UNEXPECTED_ERROR) }
                                        }
                                }
                            }
                            "MANAGER" -> {
                                val charPool = ('A'..'Z') + ('a'..'z') + ('0'..'9')
                                var password = ""
                                while (!password.matches(ValidationRegex.PASSWORD))
                                    password = (1..8).map { charPool[Random().nextInt(charPool.size)] }.joinToString("")
                                repository.store(user.copy(
                                    password = BCrypt.withDefaults().hashToString(8, password.toCharArray()),
                                    active = true,
                                    admin = false,
                                    activationDate = Date(),
                                    permissions = USER_MANAGER_PERMISSIONS
                                )).flatMap { stored ->
                                    webClient.post().uri(eNotificationsEndpoints.createManagerEndpoint())
                                        .body(BodyInserters.fromObject(ManagerCreationNotification(
                                            stored.username!!,
                                            password = password
                                        )))
                                        .exchange()
                                        .flatMap { clientResponse ->
                                            if (clientResponse.statusCode() == HttpStatus.CREATED)
                                                ResponseBuilder.createdResponse(stored.id!!)
                                            else repository.delete(stored)
                                                .flatMap { ResponseBuilder.internalErrorResponse(ERROR_UNEXPECTED_ERROR) }
                                        }
                                }
                            }
                            else -> ResponseBuilder.badRequestResponse(ERROR_UNRECOGNISED_USER_TYPE)
                        }
                    }
            } ?: ResponseBuilder.badRequestResponse(listOf(ValidationFault("username", ERROR_MISSING_USERNAME)))
        }

    /**
     * Endpoint is responsible for updating admin user fields.
     */
    fun adminUpdate(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.user.admin.update") {
            request.bodyToMono(User::class.java).flatMap { user ->
                repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), User::class.java).flatMap { storedUser ->
                    repository.store(storedUser.copy(
                        active = user.active.takeIf { it != null } ?: storedUser.active,
                        admin = user.admin.takeIf { it != null } ?: storedUser.admin,
                        permissions = USER_MANAGER_ADMIN_PERMISSIONS.takeIf { user.admin != null && user.admin }
                            ?: USER_MANAGER_PERMISSIONS.takeIf { user.admin != null && !user.admin }
                            ?: storedUser.permissions
                    )).flatMap { ResponseBuilder.okResponse(Unit) }
                }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
            }
        }

    /**
     * Endpoint is responsible for retrieving a user based on the
     * provided ID.
     */
    fun view(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.user.view") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), User::class.java).flatMap { user ->
                ResponseBuilder.okResponse(OutUser(
                    id = user.id!!,
                    username = user.username!!,
                    forename = user.forename,
                    surname = user.surname,
                    active = user.active!!,
                    admin = user.admin!!,
                    activationDate = user.activationDate
                ))
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving email addresses based
     * on the provided user ids.
     */
    fun listEmailsByIds(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.user.list") {
            repository.listByIds(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID).split(","))
                .map { it.username!! }
                .collectList()
                .flatMap { ResponseBuilder.okResponse(it) }
        }

    /**
     * Endpoint is responsible for activating new users.
     */
    fun activate(request: ServerRequest): Mono<ServerResponse> = repository
        .findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), User::class.java).flatMap { user ->
            repository.store(user.copy(active = true, activationDate = Date())).flatMap { ResponseBuilder.okResponse(Unit) }
        }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))

    /**
     * Endpoint is responsible for authenticating users and generating
     * authentication tokens.
     */
    fun authenticate(request: ServerRequest): Mono<ServerResponse> = request
        .bodyToMono(AuthenticateUser::class.java).flatMap { authenticateUser ->
            authenticateUser.password?.let { password ->
                authenticateUser.username?.let { username ->
                    repository.findByUsername(username).flatMap { user ->
                        val now = LocalDateTime.now()
                        if (!BCrypt.verifyer().verify(password.toCharArray(), user.password).verified) {
                            ResponseBuilder.unauthorizedResponse(listOf(ValidationFault("username", ERROR_FAILED_TO_AUTHENTICATE)))
                        } else if (!user.active!!) {
                            ResponseBuilder.unauthorizedResponse(listOf(ValidationFault("username", ERROR_USER_NOT_ACTIVE)))
                        } else {
                            val authenticationToken = Jwts.builder()
                                .claim("permissions", user.permissions)
                                .setId(user.id)
                                .setIssuer("oms.rmw.auth")
                                .setIssuedAt(Date.from(now.toInstant(ZoneOffset.UTC)))
                                .signWith(SignatureAlgorithm.HS256, SecurityConstants.SECURITY_KEY)
                                .compact()
                            ResponseBuilder.okResponse(authenticationToken)
                        }
                    }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
                }
                    ?: ResponseBuilder.unauthorizedResponse(listOf(ValidationFault("username", ERROR_FAILED_TO_AUTHENTICATE)))
            } ?: ResponseBuilder.unauthorizedResponse(listOf(ValidationFault("username", ERROR_FAILED_TO_AUTHENTICATE)))
        }

    /**
     * Endpoint is responsible for creating a new authentication token
     * based on a provided authentication token.
     */
    fun resetAuthToken(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.user.authentication.reset") {
            val userId = Jwts.parser().setSigningKey(SecurityConstants.SECURITY_KEY)
                .parseClaimsJws(request.headers().header("Authorization").first()).body.id
            repository.findById(userId, User::class.java).flatMap { user ->
                val now = LocalDateTime.now()
                val authenticationToken = Jwts.builder()
                    .claim("permissions", user.permissions)
                    .setId(user.id)
                    .setIssuer("oms.rmw.auth")
                    .setIssuedAt(Date.from(now.toInstant(ZoneOffset.UTC)))
                    .signWith(SignatureAlgorithm.HS256, SecurityConstants.SECURITY_KEY)
                    .compact()
                ResponseBuilder.okResponse(authenticationToken)
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    companion object {
        private const val ERROR_USERNAME_IN_USE: String = "The provided username is already in use."
        private const val ERROR_MISSING_USERNAME: String = "A username was expected but not provided."
        private const val ERROR_UNEXPECTED_ERROR: String = "An unexpected error occurred when creating the user."
        private const val ERROR_UNRECOGNISED_USER_TYPE: String = "The provided user type was missing or unrecognised."
        private const val ERROR_USER_NOT_ACTIVE: String = "User is not active."
        private const val ERROR_FAILED_TO_AUTHENTICATE: String = "Failed to authenticate the user with the provided credentials."

        private val USER_PERMISSIONS: List<String> = listOf(
            "oms.rmw.urm.user.view",
            "oms.rmw.urm.user.update",
            "oms.rmw.urm.user.delete",
            "oms.rmw.urm.user.authentication.reset"
        )

        private val USER_CUSTOMER_PERMISSIONS: List<String> = USER_PERMISSIONS + listOf(
            "oms.rmw.urm.customer.update",
            "oms.rmw.urm.customer.view",
            "oms.rmw.urm.customer.delete",

            "oms.rmw.urm.deliveryaddress.store",
            "oms.rmw.urm.deliveryaddress.update",
            "oms.rmw.urm.deliveryaddress.view",
            "oms.rmw.urm.deliveryaddress.list",
            "oms.rmw.urm.deliveryaddress.delete",

            "oms.rmw.urm.paymentdetail.store",
            "oms.rmw.urm.paymentdetail.update",
            "oms.rmw.urm.paymentdetail.view",
            "oms.rmw.urm.paymentdetail.delete",

            "oms.rmw.om.order.list.customer",
            "oms.rmw.om.order.view"
        )

        private val USER_MANAGER_PERMISSIONS: List<String> = USER_PERMISSIONS + listOf(
            "oms.rmw.urm.manager.view",
            "oms.rmw.urm.manager.update",
            "oms.rmw.urm.manager.delete",
            "oms.rmw.urm.manager.list",

            "oms.rmw.om.order.update",
            "oms.rmw.om.order.view",
            "oms.rmw.om.order.list",

            "oms.rmw.om.order.driver.assign"
        )

        private val USER_MANAGER_ADMIN_PERMISSIONS: List<String> = USER_MANAGER_PERMISSIONS + listOf(
            "oms.rmw.urm.user.list",
            "oms.rmw.urm.user.admin.update",

            "oms.rmw.urm.manager.admin.store",
            "oms.rmw.urm.manager.admin.list",

            "oms.rmw.urm.customer.list",

            "oms.rmw.om.menucategory.store",
            "oms.rmw.om.menucategory.update",
            "oms.rmw.om.menucategory.delete",

            "oms.rmw.om.menuitem.store",
            "oms.rmw.om.menuitem.update",
            "oms.rmw.om.menuitem.delete",

            "oms.rmw.om.menuitemoption.store",
            "oms.rmw.om.menuitemoption.update",
            "oms.rmw.om.menuitemoption.delete",

            "oms.rmw.om.menuitemoptiongroup.store",
            "oms.rmw.om.menuitemoptiongroup.update",
            "oms.rmw.om.menuitemoptiongroup.delete",

            "oms.rmw.om.discountcode.store",
            "oms.rmw.om.discountcode.update",
            "oms.rmw.om.discountcode.delete",
            "oms.rmw.om.discountcode.view",
            "oms.rmw.om.discountcode.list",

            "oms.rmw.om.deliveryoption.update"
        )
    }
}