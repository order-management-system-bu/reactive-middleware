package uk.ac.bournemouth.i7626893.oms.rmw.urm.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.LuhnValidator
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.PaymentDetail

/**
 * Validator class is responsible for validating inbound payment details.
 */
class PaymentDetailValidator(validateMandatoryFields: Boolean) : AbstractValidator<PaymentDetail>(validateMandatoryFields) {
    override fun validateFields(m: PaymentDetail): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("customerId")
            .takeIf { !m.customerId.isNullOrBlank() && !m.customerId!!.matches(ValidationRegex.OBJECT_ID) },
        ValidationFaults.exceededMaxLength("nameOnCard", 50)
            .takeIf { m.nameOnCard != null && m.nameOnCard.length > 50 },
        ValidationFault("cardNumber", ERROR_INVALID_CARD_NUMBER)
            .takeIf { !m.cardNumber.isNullOrBlank() && !LuhnValidator.Check(m.cardNumber!!) },
        ValidationFault("cardType", "The provided card type was unrecognised")
            .takeIf { !m.cardType.isNullOrBlank() && !listOf("VISA", "Mastercard", "American Express").contains(m.cardType!!) }
    )

    override fun validateMandatoryFields(m: PaymentDetail): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("cardType").takeIf { m.cardType.isNullOrBlank() },
        ValidationFaults.missingField("customerId").takeIf { m.customerId.isNullOrBlank() },
        ValidationFaults.missingField("nameOnCard").takeIf { m.nameOnCard.isNullOrBlank() },
        ValidationFaults.missingField("cardNumber").takeIf { m.cardNumber.isNullOrBlank() },
        ValidationFaults.missingField("cardExpiry").takeIf { m.cardExpiry == null }
    )

    companion object {
        private const val ERROR_INVALID_CARD_NUMBER: String =
            "The provided card number was not valid."
    }
}