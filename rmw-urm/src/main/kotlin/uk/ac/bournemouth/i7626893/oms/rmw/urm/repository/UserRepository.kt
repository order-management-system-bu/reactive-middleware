package uk.ac.bournemouth.i7626893.oms.rmw.urm.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.User

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the users collection.
 */
class UserRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<User>(template) {
    /**
     * Function is responsible for retrieving a single user based
     * on the provided username.
     */
    fun findByUsername(username: String): Mono<User> = template
        .findOne(BasicQuery("{\"username\": \"$username\"}"), User::class.java)

    /**
     * Function is responsible for retrieving users based on
     * the provided ids.
     */
    fun listByIds(ids: List<String>): Flux<User> = template
        .find(BasicQuery("{\"id\": {\"\$in\": ${ids.map { "\"$it\"" }}}}"), User::class.java)
}