package uk.ac.bournemouth.i7626893.oms.rmw.urm.model

import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model

/**
 * Model represents a single stored customer associated with
 * the OMS-Portal application.
 */
@Document(collection = "customers")
data class Customer(
    override val id: String? = null,
    val userId: String,
    val contactNumber: String,
    val receiveOrderUpdates: Boolean,
    val receiveNewsAndOffersEmail: Boolean,
    val receiveNewsAndOffersSms: Boolean,
    val receiveAccountNotifications: Boolean
) : Model<Customer>

/**
 * Model represents a single inbound customer.
 */
data class InCustomer(
    override val id: String? = null,
    val username: String? = null,
    val password: String? = null,
    val forename: String? = null,
    val surname: String? = null,
    val contactNumber: String? = null,
    val receiveOrderUpdates: Boolean? = null,
    val receiveNewsAndOffersEmail: Boolean? = null,
    val receiveNewsAndOffersSms: Boolean? = null,
    val receiveAccountNotifications: Boolean? = null
) : Model<InCustomer>

/**
 * Model represents a single outbound customer.
 */
data class OutCustomer(
    val id: String,
    val userId: String,
    val username: String,
    val forename: String,
    val surname: String,
    val contactNumber: String,
    val receiveOrderUpdates: Boolean,
    val receiveNewsAndOffersEmail: Boolean,
    val receiveNewsAndOffersSms: Boolean,
    val receiveAccountNotifications: Boolean
)