package uk.ac.bournemouth.i7626893.oms.rmw.urm.handler

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.toEntity
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureUpdate
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ExternalEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.DeliveryAddress
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.DeliveryAddressRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.DeliveryAddressValidator

/**
 * Handler class is responsible for exposing delivery address
 * related endpoints to external entities.
 */
class DeliveryAddressHandler(
    override val repository: DeliveryAddressRepository,
    private val cValidator: DeliveryAddressValidator,
    override val uValidator: DeliveryAddressValidator,
    private val externalEndpoints: ExternalEndpoints,
    private val webClient: WebClient
) :
    SecureUpdate<DeliveryAddress>,
    SecureView<DeliveryAddress>,
    SecureDelete<DeliveryAddress> {

    override val permission: String get() = "urm.deliveryaddress"
    override val mClass: Class<DeliveryAddress> get() = DeliveryAddress::class.java

    /**
     * Endpoint is responsible for creating a new delivery address.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.deliveryaddress.store") {
            request.bodyToMono(DeliveryAddress::class.java).flatMap { deliveryAddress ->
                val validationFaults = cValidator.validate(deliveryAddress)
                if (validationFaults.isNotEmpty())
                    ResponseBuilder.badRequestResponse(validationFaults)
                else repository.findByStreetName(deliveryAddress.customerId!!, deliveryAddress.street!!).flatMap {
                    ResponseBuilder.badRequestResponse(ValidationFault("street", "A delivery address with this street is already associated with your account."))
                }.switchIfEmpty {
                    webClient.get().uri(externalEndpoints.lookupPostcode(deliveryAddress.postcode1 + deliveryAddress.postcode2))
                        .exchange()
                        .flatMap { clientResponse ->
                            when (clientResponse.statusCode()) {
                                HttpStatus.OK -> clientResponse.toEntity<JsonNode>().flatMap { responseEntity ->
                                    if (responseEntity.body!!.get("result").get("admin_district").textValue() != deliveryAddress.city!!)
                                        ResponseBuilder.badRequestResponse(ValidationFault("city", "This city does not belong to the provided postcode"))
                                    else repository.viewDefaultAddress(deliveryAddress.customerId).flatMap { defaultAddress ->
                                        repository.store(defaultAddress.copy(defaultAddress = false)).flatMap {
                                            repository.store(deliveryAddress.copy(defaultAddress = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                                        }
                                    }.switchIfEmpty {
                                        repository.store(deliveryAddress.copy(defaultAddress = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                                    }
                                }

                                HttpStatus.NOT_FOUND -> ResponseBuilder.badRequestResponse(ValidationFault(
                                    "postcode1", "Postcode was not valid.")
                                )

                                else -> ResponseBuilder.internalErrorResponse(Unit)
                            }
                        }
                }
            }
        }

    /**
     * Endpoint is responsible for retrieving a customers default
     * address.
     */
    fun viewDefaultAddress(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.deliveryaddress.view") {
            repository.viewDefaultAddress(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID)).flatMap { deliveryAddress ->
                ResponseBuilder.okResponse(deliveryAddress)
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving all addresses
     * associated with a provided customer ID.
     */
    fun listByCustomer(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.deliveryaddress.list") {
            repository.listByCustomer(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID))
                .collectList().flatMap { ResponseBuilder.okResponse(it) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for defining a provided address
     * as a customers default.
     */
    fun assignDefaultAddress(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.deliveryaddress.update") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), DeliveryAddress::class.java).flatMap { deliveryAddress ->
                repository.viewDefaultAddress(deliveryAddress.customerId!!).flatMap { defaultAddress ->
                    repository.store(defaultAddress.copy(defaultAddress = false))
                        .flatMap { repository.store(deliveryAddress.copy(defaultAddress = true)) }
                        .flatMap { ResponseBuilder.okResponse(Unit) }
                }.switchIfEmpty {
                    repository.store(deliveryAddress.copy(defaultAddress = true))
                        .flatMap { ResponseBuilder.okResponse(Unit) }
                }
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for removing all stored addresses
     * associated with the provided customer.
     */
    fun deleteByCustomer(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.deliveryaddress.delete") {
            repository.listByCustomer(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID))
                .flatMap { deliveryAddress -> repository.delete(deliveryAddress) }
                .collectList().flatMap { ResponseBuilder.okResponse(Unit) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }
}