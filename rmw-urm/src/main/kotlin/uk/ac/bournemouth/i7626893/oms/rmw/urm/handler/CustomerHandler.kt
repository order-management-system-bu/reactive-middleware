package uk.ac.bournemouth.i7626893.oms.rmw.urm.handler

import io.jsonwebtoken.Jwts
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.toEntity
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.enotifications.CustomerModificationNotification
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.ENotificationsEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.endpoints.URMEndpoints
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.SecurityConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.Customer
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.InCustomer
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.OutCustomer
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.OutUser
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.User
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.CustomerRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.CustomerValidator
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Handler class is responsible for exposing customer related endpoints
 * to external entities.
 */
class CustomerHandler(
    private val repository: CustomerRepository,
    private val cValidator: CustomerValidator,
    private val uValidator: CustomerValidator,
    private val eNotificationsEndpoints: ENotificationsEndpoints,
    private val urmEndpoints: URMEndpoints,
    private val webClient: WebClient

) {

    /**
     * Endpoint is responsible for creating a new customer.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .bodyToMono(InCustomer::class.java).flatMap { inCustomer ->
            val validationFaults = cValidator.validate(inCustomer)
            if (validationFaults.isNotEmpty())
                ResponseBuilder.badRequestResponse(validationFaults)
            else
                webClient.post().uri(urmEndpoints.createUser())
                    .body(BodyInserters.fromObject(User(
                        type = "CUSTOMER",
                        username = inCustomer.username,
                        password = inCustomer.password,
                        forename = inCustomer.forename,
                        surname = inCustomer.surname
                    )))
                    .exchange()
                    .flatMap { clientResponse ->
                        when (clientResponse.statusCode()) {
                            HttpStatus.CREATED ->
                                clientResponse.toEntity<String>().flatMap { response ->
                                    repository.store(Customer(
                                        userId = response.body!!,
                                        contactNumber = inCustomer.contactNumber!!,
                                        receiveOrderUpdates = inCustomer.receiveOrderUpdates!!,
                                        receiveNewsAndOffersEmail = inCustomer.receiveNewsAndOffersEmail!!,
                                        receiveNewsAndOffersSms = inCustomer.receiveNewsAndOffersSms!!,
                                        receiveAccountNotifications = inCustomer.receiveAccountNotifications!!
                                    )).flatMap { ResponseBuilder.createdResponse(it.id!!) }
                                }

                            HttpStatus.BAD_REQUEST -> clientResponse.toEntity<List<ValidationFault>>().flatMap { response ->
                                ResponseBuilder.badRequestResponse(response.body)
                            }

                            else -> ResponseBuilder.internalErrorResponse(ERROR_CREATE_UNEXPECTED_ERROR)
                        }
                    }
        }

    /**
     * Endpoint is responsible for updating an existing customer.
     */
    fun update(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.customer.update") {
            request.bodyToMono(InCustomer::class.java).flatMap { inCustomer ->
                val validationFaults = uValidator.validate(inCustomer)
                if (validationFaults.isNotEmpty())
                    ResponseBuilder.badRequestResponse(validationFaults)
                else repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), Customer::class.java).flatMap { customer ->
                    if (!inCustomer.forename.isNullOrBlank() || !inCustomer.surname.isNullOrBlank() || !inCustomer.password.isNullOrBlank())
                        webClient.put().uri(urmEndpoints.updateUser(customer.userId))
                            .body(BodyInserters.fromObject(User(
                                forename = inCustomer.forename,
                                surname = inCustomer.surname,
                                password = inCustomer.password
                            )))
                            .header("Authorization", request.headers().header("Authorization").first())
                            .exchange()
                            .flatMap { clientResponse ->
                                when (clientResponse.statusCode()) {
                                    HttpStatus.OK -> webClient.get().uri(urmEndpoints.viewUser(customer.userId))
                                        .header("Authorization", request.headers().header("Authorization").first())
                                        .exchange()
                                        .flatMap { userResponse ->
                                            when (userResponse.statusCode()) {
                                                HttpStatus.OK -> userResponse.toEntity<OutUser>().flatMap { response ->
                                                    repository.store(customer.copy(
                                                        contactNumber = inCustomer.contactNumber.takeIf { !it.isNullOrBlank() }
                                                            ?: customer.contactNumber,
                                                        receiveOrderUpdates = inCustomer.receiveOrderUpdates.takeIf { it != null }
                                                            ?: customer.receiveOrderUpdates,
                                                        receiveNewsAndOffersEmail = inCustomer.receiveNewsAndOffersEmail.takeIf { it != null }
                                                            ?: customer.receiveNewsAndOffersEmail,
                                                        receiveNewsAndOffersSms = inCustomer.receiveNewsAndOffersSms.takeIf { it != null }
                                                            ?: customer.receiveNewsAndOffersSms,
                                                        receiveAccountNotifications = inCustomer.receiveAccountNotifications.takeIf { it != null }
                                                            ?: customer.receiveAccountNotifications
                                                    )).flatMap { updatedCustomer ->
                                                        webClient.post().uri(eNotificationsEndpoints.createCustomerModificationEndpoint())
                                                            .body(BodyInserters.fromObject(CustomerModificationNotification(
                                                                target_email = response.body!!.username,
                                                                full_name = "${response.body!!.forename} ${response.body!!.surname}",
                                                                date_updated = SimpleDateFormat("MM-dd-yyyy hh:mm:ss").format(Date()),
                                                                field_names = listOfNotNull(
                                                                    "Password".takeIf { !inCustomer.password.isNullOrBlank() },
                                                                    "Forename".takeIf { !inCustomer.forename.isNullOrBlank() },
                                                                    "Surname".takeIf { !inCustomer.surname.isNullOrBlank() },
                                                                    "Contact Number".takeIf { !inCustomer.contactNumber.isNullOrBlank() },
                                                                    "Order Update Notifications".takeIf { inCustomer.receiveOrderUpdates != null },
                                                                    "News and Offers Notifications".takeIf { inCustomer.receiveNewsAndOffersEmail != null || inCustomer.receiveNewsAndOffersSms != null },
                                                                    "Account Notifications".takeIf { inCustomer.receiveAccountNotifications != null }

                                                                ))))
                                                            .exchange()
                                                            .flatMap { ResponseBuilder.okResponse(Unit) }
                                                    }
                                                }

                                                HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)

                                                else -> ResponseBuilder.internalErrorResponse(ERROR_UPDATE_UNEXPECTED_ERROR)
                                            }
                                        }

                                    HttpStatus.BAD_REQUEST -> clientResponse.toEntity<List<ValidationFault>>().flatMap { response ->
                                        ResponseBuilder.badRequestResponse(response.body)
                                    }

                                    HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)

                                    else -> ResponseBuilder.internalErrorResponse(ERROR_UPDATE_UNEXPECTED_ERROR)
                                }
                            }
                    else webClient.get().uri(urmEndpoints.viewUser(customer.userId))
                        .header("Authorization", request.headers().header("Authorization").first())
                        .exchange()
                        .flatMap { clientResponse ->
                            when (clientResponse.statusCode()) {
                                HttpStatus.OK -> clientResponse.toEntity<OutUser>().flatMap { response ->
                                    repository.store(customer.copy(
                                        contactNumber = inCustomer.contactNumber.takeIf { !it.isNullOrBlank() }
                                            ?: customer.contactNumber,
                                        receiveOrderUpdates = inCustomer.receiveOrderUpdates.takeIf { it != null }
                                            ?: customer.receiveOrderUpdates,
                                        receiveNewsAndOffersEmail = inCustomer.receiveNewsAndOffersEmail.takeIf { it != null }
                                            ?: customer.receiveNewsAndOffersEmail,
                                        receiveNewsAndOffersSms = inCustomer.receiveNewsAndOffersSms.takeIf { it != null }
                                            ?: customer.receiveNewsAndOffersSms,
                                        receiveAccountNotifications = inCustomer.receiveAccountNotifications.takeIf { it != null }
                                            ?: customer.receiveAccountNotifications
                                    )).flatMap {
                                        webClient.post().uri(eNotificationsEndpoints.createCustomerModificationEndpoint())
                                            .body(BodyInserters.fromObject(CustomerModificationNotification(
                                                target_email = response.body!!.username,
                                                full_name = "${response.body!!.forename} ${response.body!!.surname}",
                                                date_updated = SimpleDateFormat("MM-dd-yyyy hh:mm:ss").format(Date()),
                                                field_names = listOfNotNull(
                                                    "Password".takeIf { !inCustomer.password.isNullOrBlank() },
                                                    "Forename".takeIf { !inCustomer.forename.isNullOrBlank() },
                                                    "Surname".takeIf { !inCustomer.surname.isNullOrBlank() },
                                                    "Contact Number".takeIf { !inCustomer.contactNumber.isNullOrBlank() },
                                                    "Order Update Notifications".takeIf { inCustomer.receiveOrderUpdates != null },
                                                    "News and Offers Notifications".takeIf { inCustomer.receiveNewsAndOffersEmail != null || inCustomer.receiveNewsAndOffersSms != null },
                                                    "Account Notifications".takeIf { inCustomer.receiveAccountNotifications != null }

                                                ))))
                                            .exchange()
                                            .flatMap { ResponseBuilder.okResponse(Unit) }
                                    }
                                }

                                HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)

                                else -> ResponseBuilder.internalErrorResponse(ERROR_UPDATE_UNEXPECTED_ERROR)
                            }
                        }
                }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
            }
        }

    /**
     * Endpoint is responsible for retrieving a single customer
     * from the applications database.
     */
    fun view(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.customer.view") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), Customer::class.java).flatMap { customer ->
                webClient.get().uri(urmEndpoints.viewUser(customer.userId))
                    .header("Authorization", request.headers().header("Authorization").first())
                    .exchange()
                    .flatMap { clientResponse ->
                        when (clientResponse.statusCode()) {
                            HttpStatus.OK -> clientResponse.toEntity<OutUser>().flatMap { response ->
                                ResponseBuilder.okResponse(OutCustomer(
                                    id = customer.id!!,
                                    userId = response.body!!.id,
                                    username = response.body!!.username,
                                    forename = response.body!!.forename!!,
                                    surname = response.body!!.surname!!,
                                    contactNumber = customer.contactNumber,
                                    receiveOrderUpdates = customer.receiveOrderUpdates,
                                    receiveNewsAndOffersEmail = customer.receiveNewsAndOffersEmail,
                                    receiveNewsAndOffersSms = customer.receiveNewsAndOffersSms,
                                    receiveAccountNotifications = customer.receiveAccountNotifications
                                ))
                            }

                            HttpStatus.NOT_FOUND -> ResponseBuilder.notFoundResponse(Unit)

                            else -> ResponseBuilder.internalErrorResponse(Unit)
                        }
                    }
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving a single customer
     * based on an authentication token.
     */
    fun viewByAuthToken(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.customer.view") {
            val userId = Jwts.parser().setSigningKey(SecurityConstants.SECURITY_KEY)
                .parseClaimsJws(request.headers().header("Authorization").first()).body.id
            webClient.get().uri(urmEndpoints.viewUser(userId))
                .header("Authorization", request.headers().header("Authorization").first())
                .exchange()
                .flatMap { clientResponse ->
                    if (clientResponse.statusCode() == HttpStatus.OK)
                        clientResponse.toEntity<OutUser>().flatMap { response ->
                            repository.findByUserId(userId).flatMap { customer ->
                                ResponseBuilder.okResponse(OutCustomer(
                                    id = customer.id!!,
                                    userId = response.body!!.id,
                                    username = response.body!!.username,
                                    forename = response.body!!.forename!!,
                                    surname = response.body!!.surname!!,
                                    contactNumber = customer.contactNumber,
                                    receiveOrderUpdates = customer.receiveOrderUpdates,
                                    receiveNewsAndOffersEmail = customer.receiveNewsAndOffersEmail,
                                    receiveNewsAndOffersSms = customer.receiveNewsAndOffersSms,
                                    receiveAccountNotifications = customer.receiveAccountNotifications
                                ))
                            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
                        }
                    else ResponseBuilder.internalErrorResponse(Unit)
                }
        }

    /**
     * Endpoint is responsible for removing a single customer
     * from the applications database.
     */
    fun delete(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.customer.delete") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), Customer::class.java).flatMap { customer ->
                webClient.delete().uri(urmEndpoints.deleteUser(customer.userId))
                    .header("Authorization", request.headers().header("Authorization").first())
                    .exchange()
                    .flatMap { clientResponse ->
                        if (clientResponse.statusCode() == HttpStatus.OK)
                            repository.delete(customer).flatMap { ResponseBuilder.okResponse(Unit) }
                        else ResponseBuilder.internalErrorResponse(Unit)
                    }
            }
        }

    /**
     * Endpoint is responsible for retrieving all customers
     */
    fun listContactableCustomers(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.customer.list") {
            repository.findContactableEmails()
                .collectList()
                .flatMap { customers ->
                    webClient.get().uri(urmEndpoints.listUserEmails(customers.map { customer -> customer.userId }))
                        .header("Authorization", request.headers().header("Authorization").first()).retrieve()
                        .bodyToFlux(String::class.java)
                        .collectList()
                        .flatMap { emails -> ResponseBuilder.okResponse(emails[0].replace("\"", "").replace("[", "").replace("]", "").split(",")) }
                        .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
                }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    companion object {
        private const val ERROR_CREATE_UNEXPECTED_ERROR: String = "An unexpected error occurred when creating the customer."
        private const val ERROR_UPDATE_UNEXPECTED_ERROR: String = "An unexpected error occurred when updating the customer."
    }
}