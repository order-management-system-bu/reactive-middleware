package uk.ac.bournemouth.i7626893.oms.rmw.urm.config

import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.CustomerHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.DeliveryAddressHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.ManagerHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.PaymentDetailHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.UserHandler

/**
 * Object is responsible for providing the URM modules
 * routing schema.
 */
object UrmRoutes {
    fun routes(
        userHandler: UserHandler,
        customerHandler: CustomerHandler,
        managerHandler: ManagerHandler,
        deliveryAddressHandler: DeliveryAddressHandler,
        paymentDetailHandler: PaymentDetailHandler
    ): RouterFunction<ServerResponse> = router {
        "/oms/rmw/urm".nest {
            "/user".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", userHandler::store)
                    POST("/authenticate", userHandler::authenticate)
                    POST("/authenticate/reset", userHandler::resetAuthToken)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/admin/update", userHandler::adminUpdate)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", userHandler::update)
                }
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}:[a-f\\d]{24}+(?:,[a-f\\d]{24}+)*}/list", userHandler::listEmailsByIds)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", userHandler::view)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/activate", userHandler::activate)
            }

            "/customer".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", customerHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", customerHandler::update)
                }
                GET("/list/email", customerHandler::listContactableCustomers)
                GET("/view", customerHandler::viewByAuthToken)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", customerHandler::view)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", customerHandler::delete)
            }

            "/manager".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", managerHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/first-time-login", managerHandler::handleFirstTimeLogin)
                }
                GET("/view", managerHandler::viewByAuthToken)
                GET("/list", managerHandler::list)
                GET("/list/delivery-driver", managerHandler::listDeliveryDriver)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", managerHandler::view)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", managerHandler::delete)
            }

            "/address".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", deliveryAddressHandler::store)
                    PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/update", deliveryAddressHandler::update)
                }
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", deliveryAddressHandler::view)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view/default", deliveryAddressHandler::viewDefaultAddress)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/list", deliveryAddressHandler::listByCustomer)
                PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/default", deliveryAddressHandler::assignDefaultAddress)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", deliveryAddressHandler::delete)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete/customer", deliveryAddressHandler::deleteByCustomer)
            }

            "/payment-detail".nest {
                accept(MediaType.APPLICATION_JSON_UTF8).nest {
                    POST("", paymentDetailHandler::store)
                }
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view", paymentDetailHandler::view)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/view/default", paymentDetailHandler::viewDefaultPaymentDetail)
                GET("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/list", paymentDetailHandler::listByCustomer)
                PUT("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/default", paymentDetailHandler::assignDefaultPaymentDetail)
                DELETE("/{${HandlerConstants.PATH_VAR_ENTITY_ID}}/delete", paymentDetailHandler::delete)
            }
        }
    }
}