package uk.ac.bournemouth.i7626893.oms.rmw.urm.model

import org.springframework.data.mongodb.core.mapping.Document
import uk.ac.bournemouth.i7626893.oms.rmw.core.model.Model
import java.util.Date

/**
 * Model represents a single stored payment detail.
 */
@Document(collection = "payment_details")
data class PaymentDetail(
    override val id: String? = null,
    val customerId: String? = null,
    val cardType: String? = null,
    val nameOnCard: String? = null,
    val cardNumber: String? = null,
    val cardExpiry: Date? = null,
    val default: Boolean? = null
) : Model<PaymentDetail>