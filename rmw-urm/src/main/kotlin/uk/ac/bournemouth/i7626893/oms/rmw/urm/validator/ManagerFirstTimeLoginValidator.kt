package uk.ac.bournemouth.i7626893.oms.rmw.urm.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.InManager

/**
 * Validator class is responsible for validating inbound managers
 * for first time login requests.
 */
class ManagerFirstTimeLoginValidator : AbstractValidator<InManager>(true) {
    override fun validateFields(m: InManager): List<ValidationFault> = listOfNotNull(
        ValidationFault("password", INVALID_PASSWORD)
            .takeIf { m.password != null && !m.password.matches(ValidationRegex.PASSWORD) },
        ValidationFaults.invalidCharacters("forename")
            .takeIf { m.forename != null && !m.forename.matches(ValidationRegex.ALPHA) },
        ValidationFaults.invalidCharacters("surname")
            .takeIf { m.surname != null && !m.surname.matches(ValidationRegex.ALPHA) }
    )

    override fun validateMandatoryFields(m: InManager): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("forename").takeIf { m.forename.isNullOrBlank() },
        ValidationFaults.missingField("surname").takeIf { m.surname.isNullOrBlank() },
        ValidationFaults.missingField("password").takeIf { m.password.isNullOrBlank() }
    )

    companion object {
        private const val INVALID_PASSWORD: String = "The provided password was invalid."
    }
}