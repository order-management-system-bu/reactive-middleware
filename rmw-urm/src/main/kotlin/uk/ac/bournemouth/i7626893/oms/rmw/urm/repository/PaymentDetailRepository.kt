package uk.ac.bournemouth.i7626893.oms.rmw.urm.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.PaymentDetail

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the payment_details collection.
 */
class PaymentDetailRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<PaymentDetail>(template) {

    /**
     * Function is  responsible for retrieving a single PaymentDetail
     * from the applications database with the provided card number.
     */
    fun findByCardNumber(customerId: String, cardNumber: String): Mono<PaymentDetail> = template
        .findOne(BasicQuery("{\"customerId\": \"$customerId\", \"cardNumber\": \"$cardNumber\"}"), PaymentDetail::class.java)

    /**
     * Function is responsible for retrieving a customers default
     * payment details.
     */
    fun findDefaultPaymentDetail(customerId: String): Mono<PaymentDetail> = template
        .findOne(BasicQuery("{\"customerId\": \"$customerId\", \"default\": true}"), PaymentDetail::class.java)

    /**
     * Function is responsible for retrieving all payment details
     * associtated with the provided customer.
     */
    fun findByCustomerId(customerId: String): Flux<PaymentDetail> = template
        .find(BasicQuery("{\"customerId\": \"$customerId\"}"), PaymentDetail::class.java)
}