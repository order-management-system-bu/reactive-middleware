package uk.ac.bournemouth.i7626893.oms.rmw.urm.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.findOne
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.DeliveryAddress

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the addresses collection.
 */
class DeliveryAddressRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<DeliveryAddress>(template) {

    /**
     * Function is responsible for retrieving all addresses
     * associated with the provided customer.
     */
    fun listByCustomer(customerId: String): Flux<DeliveryAddress> = template
        .find(BasicQuery("{\"customerId\": \"$customerId\"}"), DeliveryAddress::class.java)

    /**
     * Function is responsible for retrieving an address with the
     * provided street name.
     */
    fun findByStreetName(customerId: String, streetName: String): Mono<DeliveryAddress> = template
        .findOne(BasicQuery("{\"customerId\": \"$customerId\", \"street\": \"$streetName\"}"))

    /**
     * Function is responsible for retrieving the default address
     * associated with the provided customer.
     */
    fun viewDefaultAddress(customerId: String): Mono<DeliveryAddress> = template
        .findOne(BasicQuery("{\"customerId\": \"$customerId\", \"defaultAddress\": true}"), DeliveryAddress::class.java)
}