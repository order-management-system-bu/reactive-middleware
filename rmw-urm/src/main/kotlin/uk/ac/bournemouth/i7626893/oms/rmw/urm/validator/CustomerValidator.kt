package uk.ac.bournemouth.i7626893.oms.rmw.urm.validator

import org.apache.commons.validator.routines.EmailValidator
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.InCustomer

/**
 * Validator class is responsible for validating inbound Customers.
 */
class CustomerValidator(validateMandatoryFields: Boolean) : AbstractValidator<InCustomer>(validateMandatoryFields) {
    override fun validateFields(m: InCustomer): List<ValidationFault> = listOfNotNull(
        ValidationFault("username", INVALID_EMAIL)
            .takeIf { !m.username.isNullOrBlank() && !EmailValidator.getInstance(false).isValid(m.username) },
        ValidationFault("password", INVALID_PASSWORD)
            .takeIf { !m.password.isNullOrBlank() && !m.password!!.matches(ValidationRegex.PASSWORD) },
        ValidationFaults.invalidCharacters("forename")
            .takeIf { !m.forename.isNullOrBlank() && !m.forename!!.matches(ValidationRegex.ALPHA) },
        ValidationFaults.invalidCharacters("surname")
            .takeIf { !m.surname.isNullOrBlank() && !m.surname!!.matches(ValidationRegex.ALPHA) },
        ValidationFault("contactNumber", ERROR_INVALID_PHONE_NUMBER)
            .takeIf { !m.contactNumber.isNullOrBlank() && !m.contactNumber!!.matches(ValidationRegex.MOBILE_NUMBER) }
    )

    override fun validateMandatoryFields(m: InCustomer): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("username").takeIf { m.username.isNullOrBlank() },
        ValidationFaults.missingField("password").takeIf { m.password.isNullOrBlank() },
        ValidationFaults.missingField("forename").takeIf { m.forename.isNullOrBlank() },
        ValidationFaults.missingField("surname").takeIf { m.surname.isNullOrBlank() },
        ValidationFaults.missingField("contactNumber").takeIf { m.contactNumber.isNullOrBlank() },
        ValidationFaults.missingField("receiveOrderUpdates").takeIf { m.receiveOrderUpdates == null },
        ValidationFaults.missingField("receiveNewsAndOffersEmail").takeIf { m.receiveNewsAndOffersEmail == null },
        ValidationFaults.missingField("receiveNewsAndOffersSms").takeIf { m.receiveNewsAndOffersSms == null },
        ValidationFaults.missingField("receiveAccountNotifications").takeIf { m.receiveAccountNotifications == null }
    )

    companion object {
        private const val INVALID_EMAIL: String = "The provided email address was not valid."
        private const val INVALID_PASSWORD: String = "The provided password was invalid."
        private const val ERROR_INVALID_PHONE_NUMBER: String = "The provided contact number was not a valid mobile number."
    }
}