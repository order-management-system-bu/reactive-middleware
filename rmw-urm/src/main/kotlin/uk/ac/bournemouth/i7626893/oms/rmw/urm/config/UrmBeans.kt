package uk.ac.bournemouth.i7626893.oms.rmw.urm.config

import com.mongodb.ConnectionString
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.context.support.beans
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory
import uk.ac.bournemouth.i7626893.oms.rmw.core.CoreModule
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.CustomerHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.DeliveryAddressHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.ManagerHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.PaymentDetailHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.handler.UserHandler
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.CustomerRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.DeliveryAddressRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.ManagerRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.PaymentDetailRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.UserRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.CustomerValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.DeliveryAddressValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.ManagerFirstTimeLoginValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.PaymentDetailValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.UserValidator

/**
 * Object is responsible for providing the URM modules
 * Spring bean declarations.
 */
object UrmBeans {
    private val DB_CONNECTION_STRING: String =
        if (CoreModule.DEBUG_MODE) "mongodb://localhost:27017/oms-urm-dev"
        else "mongodb://oms-rmw:58e?PH_b?A@t*#%gj87egDN%s@ds113098.mlab.com:13098/oms-urm"

    fun beans(): BeanDefinitionDsl = beans {
        // Repository beans
        bean("urm.DatabaseTemplate") {
            ReactiveMongoTemplate(SimpleReactiveMongoDatabaseFactory(ConnectionString(DB_CONNECTION_STRING)))
        }
        bean { UserRepository(ref("urm.DatabaseTemplate")) }
        bean { CustomerRepository(ref("urm.DatabaseTemplate")) }
        bean { ManagerRepository(ref("urm.DatabaseTemplate")) }
        bean { PaymentDetailRepository(ref("urm.DatabaseTemplate")) }
        bean { DeliveryAddressRepository(ref("urm.DatabaseTemplate")) }

        // Validator beans
        bean(name = "urm.validator.user.create") { UserValidator(true) }
        bean(name = "urm.validator.user.update") { UserValidator(false) }
        bean(name = "urm.validator.customer.create") { CustomerValidator(true) }
        bean(name = "urm.validator.customer.update") { CustomerValidator(false) }
        bean(name = "urm.validator.manager") { ManagerFirstTimeLoginValidator() }
        bean(name = "urm.validator.paymentdetail.create") { PaymentDetailValidator(true) }
        bean(name = "urm.validator.deliveryaddress.create") { DeliveryAddressValidator(true) }
        bean(name = "urm.validator.deliveryaddress.update") { DeliveryAddressValidator(false) }

        // Handler beans
        bean { UserHandler(ref(), ref("urm.validator.user.create"), ref("urm.validator.user.update"), ref(), ref(), ref()) }
        bean { CustomerHandler(ref(), ref("urm.validator.customer.create"), ref("urm.validator.customer.update"), ref(), ref(), ref()) }
        bean { ManagerHandler(ref(), ref("urm.validator.manager"), ref(), ref()) }
        bean { PaymentDetailHandler(ref(), ref("urm.validator.paymentdetail.create")) }
        bean { DeliveryAddressHandler(ref(), ref("urm.validator.deliveryaddress.create"), ref("urm.validator.deliveryaddress.update"), ref(), ref()) }

        // Router beans
        bean { UrmRoutes.routes(ref(), ref(), ref(), ref(), ref()) }
    }
}