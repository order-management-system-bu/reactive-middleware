package uk.ac.bournemouth.i7626893.oms.rmw.urm.validator

import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.DeliveryAddress

/**
 * Validator class is responsible for validating inbound delivery addresses.
 */
class DeliveryAddressValidator(validateMandatoryFields: Boolean) : AbstractValidator<DeliveryAddress>(validateMandatoryFields) {
    override fun validateFields(m: DeliveryAddress): List<ValidationFault> = listOfNotNull(
        ValidationFaults.invalidCharacters("street")
            .takeIf { m.street != null && !m.street.matches(ValidationRegex.ALPHANUMERIC_W_SPACE) },
        ValidationFaults.invalidCharacters("city")
            .takeIf { m.city != null && !m.city.matches(ValidationRegex.ALPHA_W_SPACE) },
        ValidationFaults.invalidCharacters("county")
            .takeIf { m.county != null && !m.county.matches(ValidationRegex.ALPHA_W_SPACE) },
        ValidationFaults.invalidCharacters("postcode1")
            .takeIf { m.postcode1 != null && !m.postcode1.matches(ValidationRegex.ALPHANUMERIC) },
        ValidationFaults.invalidCharacters("postcode2")
            .takeIf { m.postcode2 != null && !m.postcode2.matches(ValidationRegex.ALPHANUMERIC) },

        ValidationFaults.invalidCharacters("customerId")
            .takeIf { m.customerId != null && !m.customerId.matches(ValidationRegex.OBJECT_ID) },
        ValidationFaults.exceededMaxLength("postcode1", 4)
            .takeIf { m.postcode1 != null && m.postcode1.length > 4 },
        ValidationFaults.exceededMaxLength("postcode2", 4)
            .takeIf { m.postcode2 != null && m.postcode2.length > 4 }
    )

    override fun validateMandatoryFields(m: DeliveryAddress): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("customerId").takeIf { m.customerId.isNullOrBlank() },
        ValidationFaults.missingField("street1").takeIf { m.street.isNullOrBlank() },
        ValidationFaults.missingField("city").takeIf { m.city.isNullOrBlank() },
        ValidationFaults.missingField("county").takeIf { m.county.isNullOrBlank() },
        ValidationFaults.missingField("postcode1").takeIf { m.postcode1.isNullOrBlank() },
        ValidationFaults.missingField("postcode2").takeIf { m.postcode2.isNullOrBlank() }
    )
}