package uk.ac.bournemouth.i7626893.oms.rmw.urm.handler

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureDelete
import uk.ac.bournemouth.i7626893.oms.rmw.core.handler.secure.SecureView
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.ResponseBuilder
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.handler.HandlerConstants
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.security.validateAuthenticationHeader
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.PaymentDetail
import uk.ac.bournemouth.i7626893.oms.rmw.urm.repository.PaymentDetailRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.validator.PaymentDetailValidator

/**
 * Handler class is responsible for exposing payment detail
 * related endpoints to external entities.
 */
class PaymentDetailHandler(
    override val repository: PaymentDetailRepository,
    private val cValidator: PaymentDetailValidator
) :
    SecureView<PaymentDetail>,
    SecureDelete<PaymentDetail> {

    override val permission: String get() = "urm.paymentdetail"
    override val mClass: Class<PaymentDetail> get() = PaymentDetail::class.java

    /**
     * Endpoint is responsible for creating a new payment detail.
     */
    fun store(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.paymentdetail.store") {
            request.bodyToMono(PaymentDetail::class.java).flatMap { paymentDetail ->
                val validationFaults = cValidator.validate(paymentDetail)
                if (validationFaults.isNotEmpty())
                    ResponseBuilder.badRequestResponse(validationFaults)
                else repository.findByCardNumber(paymentDetail.customerId!!, paymentDetail.cardNumber!!)
                    .flatMap { ResponseBuilder.badRequestResponse(ValidationFault("cardNumber", "A card with this card number is already associated with this account.")) }
                    .switchIfEmpty {
                        repository.findDefaultPaymentDetail(paymentDetail.customerId).flatMap { defaultPaymentDetail ->
                            repository.store(defaultPaymentDetail.copy(default = false)).flatMap {
                                repository.store(paymentDetail.copy(default = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                            }
                        }.switchIfEmpty {
                            repository.store(paymentDetail.copy(default = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                        }
                    }
            }
        }

    /**
     * Endpoint is responsible for retrieving a customers default payment
     * details.
     */
    fun viewDefaultPaymentDetail(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.paymentdetail.view") {
            repository.findDefaultPaymentDetail(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID)).flatMap { paymentDetail ->
                ResponseBuilder.okResponse(paymentDetail)
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for retrieving all payment details associated
     * with the provided customer.
     */
    fun listByCustomer(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.paymentdetail.view") {
            repository.findByCustomerId(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID))
                .collectList().flatMap { ResponseBuilder.okResponse(it) }
                .switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }

    /**
     * Endpoint is responsible for defining a provided payment detail
     * as a customers default.
     */
    fun assignDefaultPaymentDetail(request: ServerRequest): Mono<ServerResponse> = request
        .validateAuthenticationHeader("oms.rmw.urm.paymentdetail.update") {
            repository.findById(request.pathVariable(HandlerConstants.PATH_VAR_ENTITY_ID), PaymentDetail::class.java).flatMap { paymentDetail ->
                repository.findDefaultPaymentDetail(paymentDetail.customerId!!).flatMap { defaultPaymentDetail ->
                    repository.store(defaultPaymentDetail.copy(default = false)).flatMap {
                        repository.store(paymentDetail.copy(default = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                    }
                }.switchIfEmpty {
                    repository.store(paymentDetail.copy(default = true)).flatMap { ResponseBuilder.okResponse(Unit) }
                }
            }.switchIfEmpty(ResponseBuilder.notFoundResponse(Unit))
        }
}