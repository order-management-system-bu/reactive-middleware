package uk.ac.bournemouth.i7626893.oms.rmw.urm.repository

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.ac.bournemouth.i7626893.oms.rmw.core.repository.AbstractRepository
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.Customer

/**
 * Repository class is responsible for providing the necessary
 * CRUD functionality for the customers collection.
 */
class CustomerRepository(
    private val template: ReactiveMongoTemplate
) : AbstractRepository<Customer>(template) {

    /**
     * Function is responsible for retrieving a single customer
     * from the applications database based on its user ID.
     */
    fun findByUserId(userId: String): Mono<Customer> = template
        .findOne(BasicQuery("{\"userId\": \"$userId\"}"), Customer::class.java)

    /**
     * Function is responsible for retrieving customers that
     * can be contacted with promotional offers.
     */
    fun findContactableEmails(): Flux<Customer> = template
        .find(BasicQuery("{\"receiveNewsAndOffersEmail\": true}"), Customer::class.java)
}