package uk.ac.bournemouth.i7626893.oms.rmw.urm.validator

import org.apache.commons.validator.routines.EmailValidator
import uk.ac.bournemouth.i7626893.oms.rmw.core.type.ValidationFault
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationFaults
import uk.ac.bournemouth.i7626893.oms.rmw.core.utility.validation.ValidationRegex
import uk.ac.bournemouth.i7626893.oms.rmw.core.validator.AbstractValidator
import uk.ac.bournemouth.i7626893.oms.rmw.urm.model.User

private const val INVALID_EMAIL: String = "The provided email address was not valid."
private const val INVALID_PASSWORD: String = "The provided password was invalid."

/**
 * Validator class is responsible for validating inbound users
 * for update requests.
 */
class UserValidator(validateMandatoryFields: Boolean) : AbstractValidator<User>(validateMandatoryFields) {
    override fun validateFields(m: User): List<ValidationFault> = listOfNotNull(
        ValidationFault("username", INVALID_EMAIL)
            .takeIf { m.username != null && !EmailValidator.getInstance(false).isValid(m.username) },
        ValidationFault("password", INVALID_PASSWORD)
            .takeIf { m.password != null && !m.password.matches(ValidationRegex.PASSWORD) },
        ValidationFaults.invalidCharacters("forename")
            .takeIf { m.forename != null && !m.forename.matches(ValidationRegex.ALPHA) },
        ValidationFaults.invalidCharacters("surname")
            .takeIf { m.surname != null && !m.surname.matches(ValidationRegex.ALPHA) }
    )

    override fun validateMandatoryFields(m: User): List<ValidationFault> = listOfNotNull(
        ValidationFaults.missingField("username").takeIf { m.username.isNullOrBlank() },
        ValidationFaults.missingField("password").takeIf { m.password.isNullOrBlank() },
        ValidationFaults.missingField("forename").takeIf { m.forename.isNullOrBlank() },
        ValidationFaults.missingField("surname").takeIf { m.surname.isNullOrBlank() }
    )
}