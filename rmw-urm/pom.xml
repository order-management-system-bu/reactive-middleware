<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.2.RELEASE</version>
        <relativePath/>
    </parent>

    <groupId>uk.ac.bournemouth.i7626893.oms</groupId>
    <artifactId>rmw-urm</artifactId>
    <version>0.0.1</version>
    <packaging>jar</packaging>

    <properties>
        <rmw.core.version>0.0.2</rmw.core.version>

        <jackson.xml.version>2.9.8</jackson.xml.version>
        <jackson.jwt.version>0.10.5</jackson.jwt.version>
        <apache.commons.validator.version>1.6</apache.commons.validator.version>
        <bcrypt.version>0.7.0</bcrypt.version>
    </properties>

    <dependencies>
        <!-- ########################################## -->
        <!-- # RMW DEPENDENCIES                       # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>uk.ac.bournemouth.i7626893.oms</groupId>
            <artifactId>rmw-core</artifactId>
            <version>${rmw.core.version}</version>
        </dependency>
        <!-- ########################################## -->
        <!-- # LANGUAGE DEPENDENCIES                  # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
            <version>${kotlin.version}</version>
        </dependency>
        <!-- ########################################## -->
        <!-- # SPRING BOOT DEPENDENCIES               # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb-reactive</artifactId>
        </dependency>
        <!-- ########################################## -->
        <!-- # APACHE COMMONS DEPENDENCIES            # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>${apache.commons.validator.version}</version>
        </dependency>
        <!-- ########################################## -->
        <!-- # BCRYPT DEPENDENCIES                    # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>at.favre.lib</groupId>
            <artifactId>bcrypt</artifactId>
            <version>${bcrypt.version}</version>
        </dependency>
        <!-- ########################################## -->
        <!-- # JACKSON DEPENDENCIES                   # -->
        <!-- ########################################## -->
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-api</artifactId>
            <version>${jackson.jwt.version}</version>
        </dependency>
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-impl</artifactId>
            <version>${jackson.jwt.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-jackson</artifactId>
            <version>${jackson.jwt.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.module</groupId>
            <artifactId>jackson-module-kotlin</artifactId>
            <version>${jackson.xml.version}</version>
        </dependency>
    </dependencies>

    <build>
        <sourceDirectory>src/main/kotlin</sourceDirectory>
        <plugins>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <jvmTarget>1.8</jvmTarget>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>ktlint</id>
                        <phase>verify</phase>
                        <configuration>
                            <target name="ktlint">
                                <java taskname="ktlint" dir="${basedir}" fork="true" failonerror="true"
                                      classpathref="maven.plugin.classpath" classname="com.github.shyiko.ktlint.Main">
                                    <arg value="src/**/*.kt"/>
                                </java>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>ktlint-format</id>
                        <configuration>
                            <target name="ktlint">
                                <java taskname="ktlint" dir="${basedir}" fork="true" failonerror="true"
                                      classpathref="maven.plugin.classpath" classname="com.github.shyiko.ktlint.Main">
                                    <arg value="-F"/>
                                    <arg value="src/**/*.kt"/>
                                </java>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>com.github.shyiko</groupId>
                        <artifactId>ktlint</artifactId>
                        <version>0.30.0</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>
</project>