package uk.ac.bournemouth.i7626893.oms.rmw.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.support.beans
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource
import org.springframework.web.reactive.function.client.WebClient
import uk.ac.bournemouth.i7626893.oms.rmw.core.CoreModule
import uk.ac.bournemouth.i7626893.oms.rmw.om.config.OmBeans
import uk.ac.bournemouth.i7626893.oms.rmw.urm.config.UrmBeans

@SpringBootApplication
open class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args) {
        addInitializers(beans {
            bean { WebClient.builder().build() }

            bean {
                CorsWebFilter(UrlBasedCorsConfigurationSource().apply {
                    registerCorsConfiguration("/**", CorsConfiguration().apply {
                        allowedOrigins = listOf("*")
                        allowedHeaders = listOf("Content-Type", "Authorization")
                        allowedMethods = listOf("OPTIONS", "GET", "POST", "PUT", "DELETE")
                    })
                })
            }
        }, CoreModule.beans(), UrmBeans.beans(), OmBeans.beans())
    }
}